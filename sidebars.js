module.exports = {
  someSidebar: {   
	  Docusaurus: ['doc1'],
	"流程规范" :[
		"1_process/Version Specification",
		"1_process/Development Process",
		{   
		"发布流程": [ 
				"1_process/System Release Process",  
				"1_process/App Release process",
				"1_process/SDK Release Process",
				"1_process/Software Signature",
			]
		}],
		"编码规范" :[
		"2_code/System Coding Specification",
		"2_code/Java Coding Specification", 
		"2_code/Android Coding Specification", 
		"2_code/Git Submission Specification", 
		],
	"系统技术文档" :[
		"3_system/Overview Of System Technical Documents",
		  {
			"二代Glass":[
				"3_system/2ndglass/2nGlass Compilation Guide", 
				"3_system/2ndglass/2nGlass Version Information"
			],
			"三代Glass":[
				"3_system/3rdglass/3nGlass Compilation Guide",  
			],
			"中油头环":[
				"3_system/5thglass/Compilation Guide For Zhongyou Head Ring",  
				"3_system/5thglass/Guide To Code Modification Of Zhongyou Head Ring",  
			],
			"技术文档":[
				"3_system/AR Projection",   
				"3_system/Gerrit Sync Repo",   
				"3_system/CloudService/CloudService Architecture And Usage",  
				"3_system/GlassUpgrade/Glass Camera Firmware Upgrade Process",    
			],
		  }
	],

	"App技术文档":[ 
		"4_app/App Overview Of Technical Documents", 
		{
			"App":[
				"4_app/AppLauncher",
				"4_app/App Collaboration",
				"4_app/App Appstore",
			],
			"技术分享":[
				"4_app/Flutter Learning",
			],
			"竞品分析":[ 
				"4_app/1_product/Multi person cooperation in competitive product analysis",
				{
					"AR设备":[
						"4_app/2_ar_device/realwear",
					]
				}
			]
		}
	],

	"SDK技术文档":[
		"5_sdk/SDK Overview of technical documents"
	],

	"工具":[
		"6_tools/Rokid Gradle Plugin",
		"6_tools/HiTool"
	],
 
	"BSP":[ 
		"7_bsp/Bsp development report of second generation glasses",
		"7_bsp/Ge2d module guide",
		"7_bsp/Summary of BSP development of Hisilicon",
		"7_bsp/Compatibility test report of second generation glasses Bluetooth noise reduction headset",
		"7_bsp/Transplantation and development of EC20 on glass2",
		"7_bsp/Version number and upgrade rules of split glasses", 
	],
  }
};
