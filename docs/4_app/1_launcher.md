---
id: AppLauncher
title: Launcher
--- 


显示桌面应用列表，应用安装时，可以查询到应用的当前状态
## 流程图
## 数据字典
### Launcher table
| 字段名称 | 字段类型 | 字段含义| 描述|
| :--- | :---- | :---- |  :---- | 
|id|Integer|自动增长 ID| 主键|
|appName	|String	|应用名称	||
|packageName	|String	|应用包名	|应用信息可以通过 packageName 获取|
|appType	|Integer | 应用类型: <br></br>0:普通 <br></br>1:强制(强制或者系统应用)||
|orderId	|Integer	|排序序号	|应用排序序号|
|visible	|Integer	|应用可见性：<br></br>0:可见 <br></br>1:不可见|	非强制应用可设置可见性|