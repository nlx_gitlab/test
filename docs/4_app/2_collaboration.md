---
id: App Collaboration
title: 多人协作
--- 

 
目前使用了菊风的会控SDK，实现了创建、加入会议，共享摄像头，共享屏幕，白板标注，截图标注等功能，下面主要介绍这些功能的主要流程，目前在 Android 手机端，Rokid Glass 端，Win PC上实现了这些功能。
## 概念
* 码率：比特率，编码器每秒编出的数据大小，单位是kbps，影响画面清晰度
* 帧率：FPS，每秒钟有多少帧画面，影响画面流畅度
* 编码格式：YUV 和 RGB，协作中需要的是YUV
* SVC：菊风设置码率层级的配置

## 业务流程
整体的流程  
![](../media/collaboration/col_logic.png ':size=600x480')

### 登录
所有的功能，都依赖与菊风云服务的登录，只有登陆成功后才能进行平台上的各种业务，三端在打开应用后，进行 login 操作。  
账号规则：
* Glass端：G_ + SN的后8位
* Win PC端：W_ + mac地址
* Android端：A_ + ANDROID_ID

?> 这里的账号登录，不是真正的传统账号概念，相当于设备的唯一识别

### 创建会议 
![](../media/collaboration/login.png ':size=600x535')   
三端创建会议(Glass端在二期中增加)，config中几个重要的参数： 
* **CONFIG_MAX_SENDER**:<br></br>
设置会议最⼤大的上⾏行行路路数(能发送媒体数据，1~16)，默认16路，发起会议时设置。   
//最新反馈的，MAX_SENDER设置为64，(16+48)，16发送媒体数据，48只能发送音频
* **CONFIG_CAPACITY**:<br></br>
设置会议⼈人数，默认8⼈，发起会议时可设置
* **CONFIG_SVC_RESOLUTION**: <br></br>
默认值：`1 144 150 288 450 720 1350`  
现在设置参数： `1 1072 2000 1072 6000`，这里设置两层以上，视频硬编码(菊风SDK)才会生效
* **CONFIG_HEART_TIME** <br></br>
设置会议心跳间隔
* **CONFIG_HEART_TIMEOUT** <br></br>
设置会议心跳超时时间

三端的创建会议需要进行统一：
*  CONFIG_MAX_SENDER： 64
*  CONFIG_CAPACITY： 264

!> 1. SVC控制不同码率下的每一层分辨率，第一个数字1是倍数。后面跟着成对的两个数字，144代表144p的分辨率，150代码相应的码率。一共有4层，分别代表requestVideo时候的Resolution.Min, Small, Large 和 Max。   
2. 设置最小发送/接收估计码率，设置的初始码率，和在通话过程中，码率上升的速度有关  
MtcConfDb.Mtc_ConfDbSetMinRecvBwe(3000);   
MtcConfDb.Mtc_ConfDbSetMinSendBwe(3000);   
设置合理的最小、最大和初始码率   
3. 容量: **16(音视频)+48(音频)+200(观众)**

### 加入会议
加入会议有两种方式，输入会议号码和扫码入会
### 离开会议
现在使用的离开会议的操作，并不是结束会议。结束会议，只有主持⼈人才能调⽤        
离开会议，会议中全部成员离开后，会议还会保留3分钟    

### 共享摄像头
目前共享摄像头、共享屏幕、白板标注、截图标注都是互顶的机制，二期中摄像头共享去除互顶机制。    

![](../media/collaboration/camera_share.png)

### 共享屏幕
共享屏幕的逻辑和共享摄像头，整体逻辑基本一致。   
![](../media/collaboration/screen_share.png)

### 白板标注
白板涂鸦，底层的实现就是通过消息发送来实现，目前涂鸦标注也互顶的机制。互顶的逻辑，这里就不在累述。主要看下涂鸦的实现原理。   
![](../media/collaboration/doodle_principle.png)
### 截图标注
截图标注，只有摄像头共享的时候可用，选择一个摄像头共享的画面，进行截图标注。    

![](../media/collaboration/snapshot_share.png)

## 实例分析
主要分析下眼镜端摄像头共享的时候，PC 或者 Android 端接收的统计信息，有助于测试同学定位问题。  
统计日志中关注，NetWork和Participants    
----
眼镜端：
``` shell
*********Network*********
Send Statistic:
    Packets:       15541|916|0|0
    RTT:           6
    Jitter:        0
    Lost:          0
    LostRate:      0
    RelayLost:     0
    BitRate/BWE:   3037/3000
    AudioResend:   0|0
    VideoResend:   0|0
    ScreenResend:  0|0
    MaxPredKbps:   7535
Server(4771379226481255):
A_ef81da3*24e106fdca(1): BWE(393|7843) IN(A:17;V:7825=G_17000068[0f00]7825)
G_17000068(2): BWE(1587|3000) OUT(A:4b;FPS:24,FEC:20,SUB:0f00=A_ef81da3*24e106fdca|0f00) IN(A:0;)

 *********Participants*********
UserId:A_ef81da3bccbb0606133d8024e106fdca
Subscribe Stats:
    Audio:          true

UserId:G_17000068
Audio Sending Stats:
    Packets:        866             
    BitRate:        16.7            
    FecPrecent:     0
Video Sending Stats:
    Packets:        14400           
    Capture Fr:     27              
    FPS/IDR:        11/1            
    Resolution:     1904x1072[0|3]  
    Bitrate/Setrate:2772/8000       
    QP:           0               
    FecPrecent:     20
Be Subscribed Stats:
    Audio:          true
    Video:          [0|F|0|0]
    Screen:         [0|0|0|0]
```
* Network
    * Send Statistic
        * BitRate/BWE: 3872/3000      //当前发送的码率/设置的发送初始码率

* Participants
    * Video Sending Stats
        * Capture Fr: 24                //Camera采集的帧率
        * FPS/IDR:    10/1              //视频发送的帧率
        * Resolution: 1904x1072[0|3]    //视频的分辨率，处于哪一档，质量
        * Bitrate/Setrate:3112/8000     //视频发送的码率/所有档的码率总和

?> 1. BWE参数，可以查看设置是否生效   
2. 视频发送的帧率和码率，可以查看上升的速度和质量

Android 端：
``` shell
*********Network*********
Recv Statistic:
    Packets:       15561|1577|0|22
    Jitter:        2
    Lost:          22
    Lost Ratio:    0
    BitRate/BWE:   2118/7843
Server(4771379226481255):
A_ef81da3*24e106fdca(1): BWE(393|7843) IN(A:17;V:7825=G_17000068[0f00]7825)
G_17000068(2): BWE(1587|3000) OUT(A:4b;FPS:24,FEC:20,SUB:0f00=A_ef81da3*24e106fdca|0f00) IN(A:0;)

*********Participants*********
UserId:A_ef81da3bccbb0606133d8024e106fdca
Video Sending Stats:
    Packets:        0               
    Capture Fr:     N/A             
    FPS/IDR:        0/0             
    Resolution:     N/A             
    Bitrate/Setrate:0/0             
    QP:           0               
    FecPrecent:     20
Be Subscribed Stats:
    Audio:          true
    Video:          [0|0|0|0]
    Screen:         [0|0|0|0]

UserId:G_17000068
Video Receiving Stats:
    Packets:        13855           
    Bitrate:        3122            
    FPS/FIR:        10/0            
    Resolution:     1904x1072       
    Render FR:      11              
    PvMos:          3.9             
    SMOS:           5
Subscribe Stats:
    Audio:          true
    Video:          [0|F|0|0]
```
Android 接收端的参数，主要和发送端进行对应    
* NetWork
    * Recv Statistic
        * BitRate/BWE:   2118/7843  //当前接收到的码率

* Participants
    * Video Receiving Stats
        * Bitrate:  3122  //视频接收到的码率
        * FPS/FIR:  10/0  //视频接收到的帧率
        *  Resolution: 1904x1072    //视频分辨率

?> 主要看视频接收到的码率和帧率，检查当前视频的质量