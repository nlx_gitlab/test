---
id: Multi person cooperation in competitive product analysis
title: 多人协作
--- 

# 远程协作竞品分析
分析其他远程协作相关的产品，并和公司当前的多人协作进行对比分析。主要对比了三家国外远程协作平台：[Oculavis](https://oculavis.de/en/home/)，[Scope AR WorkLink](https://www.scopear.com/)，[Augmentir](https://www.augmentir.com/)。   

## Oculavis
[Oculavis](https://oculavis.de/en/home/)是2016年成立的德国高科技企业，其中核心产品Oculavis SHARE是一个为机械设备制造商提供远程服务和维护的解决方案。  

![](../../media/collaboration/oculavis.png ':size=800x500')
### 功能
#### 远程协作
* 高清的音视频通话
* 多对多通话
* AR 标注
* 截图白板标注
* <font color='red'>远程控制相机、麦克风、闪光灯等一些硬件</font>

#### AR工作指导
* 文字、图片和视频的说明
* 轻松建模
* 支持3D AR内容
* "X-Ray"查看机器内部，进行高级故障诊断和硬件购买

### 特色
* 数据管理   
屏幕截图、录像保存，灵活的应用程序接口可将企业现有的IT系统与oculavis SHARE集成，在机器和系统发生严重故障的情况下，远程专家还可以从相关机器访问实时和过去的数据
* 资产识别   
<font color='red'>通过二维码或者蓝牙识别机械设备，在现场提供文档和分步指导</font>
* 角色权限控制   
为团队和用户定义权限，可与第三方用户系统进行对接
* 软件全端适配   
Web,Native App,眼镜

## Scope AR WorkLink
[Scope AR WorkLink](https://www.scopear.com/)主打两个功能：远程协助和AR培训指导   

![](../../media/collaboration/scopear.png ':size=800x430')
### 功能
#### 远程协助
* AR实时标注
* 低带宽模式：网络较差的情况，发送快照图片替代实时视频
* 联系人列表：支持一键联系专家
* Precision View：截图标注，可以操作缩放进行标注，可存放多个历史标注，与实时视频之间来回切换

#### AR培训指导 
* WorkLink Create允许任何人编写清晰，易于遵循的AR说明
* 直观的AR创作软件
* AR工作指导书  
   * 分步说明
   * 用户数据
   * GPS 

### 特色
* 自建了一套简易可视化后台工具，无需编程经验即可快速构建一个AR培训指导应用
* 软件支持 Native App和眼镜(HoloLens和爱普生等AR眼镜)

## Augmentir
[Augmentir](https://www.augmentir.com/)是一家以AI为核心的工业协作平台，其拥有一套简化版的工作说明文档创建工具

![](../../media/collaboration/augmentir.png ':size=800x500')
### 功能
#### 远程协助
* 音视频通话
* 文件共享和标注
* 一对多协作
#### AR工作指导
* 拖放交互式组件
* 将图纸链接或者图片加入到工作说明中
* 将远程专家加入到分步说明中
* 轻松集成第三方业务系统
* 支持组织权限

#### <font color='red'>AI驱动的运营</font>
!> 嵌入式AI引擎使用丰富的活动数据流实现了真正的组织优化，从而洞察了运营，持续改进，质量，培训，知识管理和工程方面各利益相关方的最大商机。
### 特色
* 加入人工智能和机器学习，对数据进行整理
* 软件支持任何移动设备或可穿戴设备

## 对比
目前分析的三家公司，都有远程协作和AR工作指导，Rokid没有做AR工作指导相关功能

|  功能 | Rokid | Oculavis| Scope AR WorkLink|Augmentir|
| :--- | :---- | :---- |  :---- | :---- | 
|**<font color='red'>高清音视频</font>**|支持|支持| 支持| 支持|
|**<font color='red'>截图标注</font>**|支持|支持|支持|支持|
|**<font color='red'>AR实时标注</font>**|不支持|支持|支持|支持|
|**<font color='red'>联系人列表</font>**|不支持|支持|支持|支持|
|硬件控制|不支持|支持|不支持|不支持|
|屏幕共享|支持|不支持|不支持|不支持|
|白板标注|支持|不支持|不支持|不支持|
|终端适配|Android<br></br>PC(Win)<br></br>Rokid眼镜|<br></br>Web<br></br>Android、iOS<br></br>眼镜(Realwear、Epson等)|<br></br>Android、iOS<br></br>眼镜(HoloLens、Epson等AR智能设备)|<br></br>任何移动设备<br></br>启用AR的智能眼镜|
|私有化部署|支持|未知|未知|未知|

## 总结
目前分析的几家远程协作平台，功能上大同小异。主要的核心功能是<font color='red'>实时音视频，AR标注，2D/3D信息展示等</font>。  
其他功能都是围绕核心功能去扩展，增加云端服务，AR指导内容制作，简化AR操作难度等。  
通过分析国外的几家AR远程协作平台，目前Rokid远程协作，<font color='red'>高清音视频</font>，<font color='red'>截图标注</font>等几个主要功能已经具备，不过其他几家都有相关<font color='red'>联系人列表，AR内容制作，并有分步指导流程等</font>，在这方面，我们还有比较长的要走。   
后面还需要和国内相关平台(好视通、软铸、联想iAid)再进行对比，分析出目前在远程协作平台上的核心竞争力。

## 参考
* [AR远程协作平台汇总：视频通讯、AR标注最核心](https://zhuanlan.zhihu.com/p/109553589)
* [Oculavis share——基于增强现实的设备远程运维解决方案](http://www.zijin.net/news/tech/1429973.html)
* [有哪些好用的互联网项目远程协作工具？](https://www.zhihu.com/question/29144478)
* [Oculavis](https://oculavis.de/en/home/)
* [Scope AR WorkLink](https://www.scopear.com/)
* [Augmentir](https://www.augmentir.com/)