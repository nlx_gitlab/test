---
id: App Appstore
title: 应用市场
--- 
 
## 功能列表
应用市场的功能列表：

![](../media/appstore/fun.png)   
主要功能：
* 应用下载
* 应用更新
* 应用管理
    * 排序
    * 隐藏
    * 卸载

### 应用下载
### 应用更新
### 应用管理
#### 排序
#### 隐藏
#### 卸载
## 数据字典
主要分为下载表和Launcher表(用于显示Launcher)  
结构存放在Launcher应用中，应用市场通过ContentProvider操作Launcher数据
### 下载表
| 字段名称 | 字段类型 | 字段含义| 描述|
| :--- | :---- | :---- |  :---- | 
|id|Integer|自动增长 ID| 主键|
|appName	|String	|应用名称||
|appIcon	|String	|应用图标||
|appSize	|Integer	|应用大小||
|appVersion	|Integer	|应用版本||
|appVersionName	|String	|应用版本显示名称||
|appType	|Integer | 应用类型: <br></br>0:普通<br></br>1:强制||
|packageName	|String	|应用包名||
|updateTime|	Long	|应用更新时间||
|downloadUrl	|String	|下载地址||
|desc	|String|	应用描述||
|detailUrl	|String	|详情页面||