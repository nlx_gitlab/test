---
id: Software Signature
title: 软件签名
--- 
 
软件签名，目前分为非系统应用签名和系统签名。  
## 非系统应用签名
* 非系统应用签名文件
[rokid_app.jks](https://gitlab.rokid-inc.com/glass/Doc/raw/develop/docs/media/sign/rokid_app.jks ':target=_blank')
* 配置信息
``` gradle
signingConfigs {
    release {
        storeFile file("rokid_app.jks")
        storePassword 'android'
        keyAlias 'android'
        keyPassword 'android'
    }
}
```

## 系统签名
系统应用签名
