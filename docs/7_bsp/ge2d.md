---
id: Ge2d module guide
title: GE2D模块使用指南
--- 
 

## 简介

GE2D是Amlogic系列芯片内部提供的2D图形加速模块的简称，该模块能够脱离CPU，独立完成基础的图形学操作，能够有效的降低CPU占用。



- 根据 Amlogic-Figo提供的信息，GE2D能够完成如下实际使用需求：
  1. 颜色格式转换 (例如:RGB -> NV21)
  2. 图片裁剪 （例如:从1920*1080的图片中裁剪一块尺寸可变的图片）
  3. 内存搬运
  4. 图片缩放 



- GE2D为应用程序提供了用户态调用接口：libge2d(代码目录：vendor/amlogic/common/system/libge2d)。该目录下提供了ge2d_feature_test.c（用户态功能测试命令行程序），作为集成libge2d功能的参考，后续使用指南也以ge2d_feature_test 作示范。



- 注：进行下列测试之前，需要将ge2d_feature_test 推至 /system/bin,命令参考如下：

  ```shell
  adb root;adb remount
  adb push ge2d_feature_test /system/bin
  adb shell chmod +x /sysmtem/bin/ge2d_feature_test
  ```

  



## 颜色格式转换命令示例

```shell
./ge2d_feature_test --op 2 --src1_size 1920x1080 --dst_size 1920x1080 --src1_format 1 --dst_format 3 --strechblit 0_0_1920_1080-0_0_1920_1080 --src1_file ./source.rgb32 --dst_file ./output.rgb24 --src1_memtype 0 --src2_memtype 0 --dst_memtype 0
```

注：src1_format 与 dst_format实际值需要根据实际RAW图片的格式来确定，对应关系参见附录1。



## 图片裁剪转换命令示例

```shell
ge2d_feature_test --op 2 --src1_size 1920x1080 --dst_size 1280x720 --src1_format 1 --dst_format 1 --strechblit 10_10_1280_720-0_0_1920_1080 --src1_file ./source.rgb32 --dst_file ./output.rgb32 --src1_memtype 0 --src2_memtype 0 --dst_memtype 0
```





## 图片放大转换命令示例

```shell
./ge2d_feature_test --op 2 --src1_size 1280x720 --dst_size 1920x1080 --src1_format 1 --dst_format 1 --strechblit 0_0_1280_720-0_0_1920_1080 --src1_file ./source.rgb32 --dst_file ./output.rgb32 --src1_memtype 0 --src2_memtype 0 --dst_memtype 0
```



## 图片缩小转换命令示例

```shell
./ge2d_feature_test --op 2 --src1_size 1920x1080 --dst_size 1280x720 --src1_format 1 --dst_format 1 --strechblit 0_0_1920_1080-0_0_1280_720 --src1_file ./source.rgb32 --dst_file ./output.rgb32 --src1_memtype 0 --src2_memtype 0 --dst_memtype 0
```





## 附1：颜色格式参数对应表

```c

vendor/amlogic/common/system/libge2d/include/ge2d_port.h

/**
 * pixel format definitions
 */
typedef enum  {
    PIXEL_FORMAT_RGBA_8888          = 1,
    PIXEL_FORMAT_RGBX_8888          = 2,
    PIXEL_FORMAT_RGB_888            = 3,
    PIXEL_FORMAT_RGB_565            = 4,
    PIXEL_FORMAT_BGRA_8888          = 5,
    PIXEL_FORMAT_YV12               = 0x32315659, // YCrCb 4:2:0 Planar  YYYY......  U......V......
    PIXEL_FORMAT_Y8                 = 0x20203859, // YYYY
    PIXEL_FORMAT_YCbCr_422_SP       = 0x10, // NV16   YYYY.....UVUV....
    PIXEL_FORMAT_YCrCb_420_SP       = 0x11, // NV21   YYYY.....UV....
    PIXEL_FORMAT_YCbCr_422_UYVY        = 0x14,     // UYVY   U0-Y0-V0-Y1 U2-Y2-V2-Y3 U4 ...
    PIXEL_FORMAT_BGR_888,
    PIXEL_FORMAT_YCbCr_420_SP_NV12,                // NV12 YCbCr YYYY.....UV....
}pixel_format_t;
```



Owner:Xinyu.zhu(xinyu.zhu@rokid.com)

