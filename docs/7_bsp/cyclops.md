---
id: Bsp development report of second generation glasses
title: 二代眼镜BSP开发报告
--- 

# 		二代眼镜 平台开发工作总结	

二代眼镜项目临近收尾，回顾接近1年的平台开发工作，共产生了370余次代码改动提交，现对平台开发工作进行总结与归纳，分析总结重点问题，总结开发经验。

## 电源管理

### #1

关键词：关机充电模式 Healthd 电源管理 充电控制

类型：BugFix

问题描述：关机充电模式未启动Android电源管理相关的服务程序healthd，导致系统在进入关机充电模式后拔出充电器无法自动关机。

解决方案：在关机充电模式下，启用healthd服务监控电池与充电状态。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16305/



### #2

关键词：关机充电模式 Healthd 电源管理 充电控制

类型：BugFix

问题描述：在Android系统中电池与充电器状态是非常重要的，在非移动设备平台中，会通过注册虚假的电池驱动与充电器驱动保持系统的正常运行，当我们需要增加实际存在的电量计/Charger驱动时，需要将Dummy Battery/Charge驱动移除，以免系统无法获知正确的电量/充电状态。

解决方案：移除Dummy Battery/Charger驱动。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17422/



### #3

关键词： 电源管理 充电控制

类型：Feature

问题描述：Android系统中的healthd服务需要在power_supply/battery节点下的status中获取当前电池的充电状态。

解决方案：补全/power_supply/battery/status节点。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17526/



### #4

关键词：关机充电 Uboot 电源管理 充电控制

类型：Feature

问题描述：在系统处于关机状态下，连接充电器后，系统应自动启动并进入关机充电模式（Kernel完全启动，Android系统仅启动部分必须的服务，例如Healthd），并对充电状态持续进行监测和控制。

解决方案：在Uboot中通过判断MCU传递的状态信号，选择进入关机充电模式 或 正常启动模式。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17542/ / https://openai-corp.rokid.com/#/c/17811/



### #5

关键词：PowerManagerMCU 电量显示

类型：Feature

问题描述：Soc通过I2C将当前的电量传递至PowerManagerMCU，增加相关驱动以支持该功能。

解决方案：增加驱动支持功能。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17565/



### #6

关键词：关机控制 漏电

类型：Feature

问题描述：为了避免在设备关机后仍存在较大的关机漏电流，需依次关闭DC-DC Enable 和 BQ25890 Charger BATFET。

解决方案：在BQ25890驱动中增加BATFET控制接口，在PowerManagerMCU驱动中增加对与DC-DC Enable的控制接口，在系统进入关机流程时调用。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17596/



### #7

关键词：Charger

类型：Feature

问题描述：修正Charger充电截止电压，充电最大电流，反向Boost输出电压以适配力科电池（二代项目选用的电池）。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17912/



### #8

关键词：电量计

类型：Feature

问题描述：更新电量计参数，该参数来自于电量计厂商，获取该参数的流程为：联系代理商->电池送样->原厂释放电池参数

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18057/



### #9

关键词：Charger 充电策略

类型：Feature

问题描述：优化充电策略，以平衡2代Dock充电情况下的散热与充电时间需求。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18163/



### #10

关键词：Charger

类型：BugFix

问题描述：测试发现偶现连接充电器的情况下，Dock出现了指示灯变黄，进入休眠唤醒状态，排查发现原先将WakeLock与充电状态绑定的方式会导致充电过程中/充电完成后出现偶然的解锁情况，导致进入休眠状态，后续将逻辑修改为WakeLock与充电器的输入电压是否存在绑定。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/21302/



### #11

关键词：电量计

类型：Feature

问题描述：由于充电曲线的问题，当电池电量达到95%时，充电速度由于Charger输出电压与电池电压压差过小而降低，在进行5%的充电时充电时间需要约40分钟，为了让用户不感觉充电速度过慢，在电量达到95%时，将其映射为100%。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20675/



### #12

关键词：电量计

类型：BugFix

问题描述：自测发现当Dock进入低电量状态（系统电量低于5%）后再充电使其电量恢复至50%以上后，仍回导致电量计持续产生低电量中断。

解决方案：排查发现在电量计驱动中响应低电量中断的处理函数中未正常的清除低电量中断的标志位，导致电量计芯片会持续触发低电量中断，修正中断处理函数中的错误逻辑后恢复正常。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20675/



### #13

关键词：Charger BoostMode

类型：BugFix

问题描述：研发反馈在同时连接眼镜和红外模组时，眼镜与红外模组均无法正常工作。

解决方案：排查后发现Charger BoostMode对最大电流进行了限制，将限制值修改为2.4A后修复该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/19887/



## WiFi/BT

### #1

关键词：WiFi模组驱动

类型：Feature

问题描述：增加Cypress CM256SM模组驱动支持。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16305/



### #2

关键词：WiFi模组驱动

类型：BugFix

问题描述：WiFi WakeUp GPIO配置异常，导致WiFi发送的中断信号无法被Soc收到，模组无法正常工作。

解决方案：修正WiFi WakeUp GPIO配置。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16307/



### #3

关键词：WiFi模组驱动

类型：BugFix

问题描述： Cywdhd WiFi驱动在平台迁移至Android9后无法正常工作。

解决方案：在Android9平台上适配WiFi驱动。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16814/



### #4

关键词：WiFi模组驱动

类型：BugFix

问题描述：切换默认启用WiFi驱动为CM256SM。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16815/



### #5

关键词：WiFi模组驱动

类型：BugFix

问题描述：WiFi驱动在关闭后再次打开会产生异常，该问题是有于未启用“ENABLE_INSMOD_NO_FW_LOAD”特性即每次启动驱动均加载WiFi Firmware导致。通过在Makefile中启用该配置文件以修复该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17336/



### #6

关键词：Bluetooth OPP 文件传输

类型：BugFix

问题描述：眼镜手机APP无法通过蓝牙向设备传送文件，检查日志后发现蓝牙Profile中的OPP（Object Push Profile）未启用。

解决方案：在Bluetooth Stack中启用OPP Profile.

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17336/

### #7

关键词：Bluetooth Firmware

类型：BugFix

问题描述：自测发现蓝牙连接稳定性较差，传输文件经常断开，排查发现是Bluetooth Stack向蓝牙芯片加载了错误的固件，导致其功能表现不正常。

解决方案：在配置文件中指定蓝牙固件编号。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17718/



### #8

关键词：Bluetooth MacAddress

类型：BugFix

问题描述：自测发现每一台设备的蓝牙Mac地址都一样，排查发现系统未使用模组中预置的Mac地址。

解决方案：修改蓝牙协议栈配置，使其从Bluetooth模组中取出预置的Mac地址。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18065/



### #9

关键词：WiFi

类型：BugFix

问题描述：自测发现WiFi模组在屏蔽房中进行打流测试（吞吐量测试）时，模组的TX/RX吞吐量均较低。

解决方案：修改WiFi Bcmdhd驱动，以提升WiFi性能。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18904/ / https://openai-corp.rokid.com/#/c/19521/



### #10

关键词：WFD WiFi Framework

类型：BugFix

问题描述：在测试过程中发现，设备通过WFD功能与投屏器建立连接后，建立的连接是2.4G，其性能也较差。

解决方案：修改WFD参数：GroupOwnerLevel，修改后，设备成为GroupOwner建立5G通信链路，性能有所提升。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18937/ 



### #11

关键词：WiFi CountryCode 吞吐量

类型：BugFix

问题描述：测试发现通过Android上层入口（原生设置/扫一扫等）配置WIFI网络连接后，通过Iperf工具测试吞吐量仅为30Mb/s，相反直接调用wpa_suplicant的接口进行WIFI网络连接，测试吞吐量可达到200Mb/s（5GWIFI 80Mhz）。通过回溯版本后发现调整WiFi CountryCode为CN/01后导致了吞吐量的下降，之后确认CN/01并不是对应国内WIFI信道配置，更正为CN/38后修复该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/21233/



### #12

关键词：WiFi suspend

类型：BugFix

问题描述：测试发现当通过Android系统的设置入口关闭WiFi后，会导致无法正常进入休眠状态，分析日志后发现上层Android系统关闭WiFi后，WiFi驱动在进入休眠状态时执行dhdsdio_suspend未对网卡是否开启作判断（未开启状态下网卡部分信息处于未初始化的状态）。

解决方法：在dhdsdio_suspend中增加对网卡是否开启的判断，当网卡处于未开启的状态时，直接返回成功。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20640/



## 音频

### #1

问题描述：增加USB Audio HAL 支持。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16748/



### #2

问题描述：Amlogic Audio 子系统 Pinctrl配置异常，导致眼镜无法正常播放音频。

解决方案：修正Audio子系统Pinctrl配置。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16746/



### #3

问题描述：适配Rokid Glass USB Audio驱动。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16747/



### #4

关键词：Amlogic Audio

类型：BugFix

问题描述：原Device Builtin 声卡中包含Capture属性，可能导致应用“语音助手”在眼镜未插入的情况下打开错误的声卡，导致其功能失效。

解决方案：移除Builtin声卡中的Caputre属性。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17655/ / https://openai-corp.rokid.com/#/c/17777/

​									 

## 显示

### #1

关键词：LT9721 PowerControl

类型：BugFix

问题描述：为了降低功耗，在未连接眼镜的情况下，需要通过MOSFET关闭LT9721 IC的芯片，以降低该场景下的功耗（降低越500mW）。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18781/ 



### #2

关键词：LT9721 PowerControl

类型：BugFix

问题描述：为了确保LT9721 Displayport Briage在每次断开后再次连接都能正常显示，在

断开连接执行init函数确保芯片恢复正常。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18663/ 



### #3

关键词：LT9721 Tune

类型：BugFix

问题描述：硬件测试反馈，在进行DisplayPort眼图测试时，发现DOCK输出的DisplayPort信号质量较差。

解决方法：优化LT9721参数，使其输出的信号能够达到信号测试标准。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20406/ 



### #4

关键词：LT9721 花屏

类型：BugFix

问题描述：测试发现在连接眼镜后，偶现会出现眼镜显示花屏的问题。

解决方法：分析后确认该问题为LT9721的FrameFIFO中在显示准备好之前可能存在错乱数据，通过在启用显示输出之前，执行Clear FIFO以修复该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20018/ 



### #4

关键词：LT9721 花屏 插入损耗

类型：BugFix

问题描述：测试发现在连接眼镜后，偶现会出现眼镜显示无法正常工作的问题。

解决方法：协同硬件测试@晓慧一同分析后确认该问题是由于DOCK配套的TypeC线以及眼镜上的FPC损耗较高导致，通过调整LT9721输出信号的增益以修复该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20003/ 

## 系统稳定性&功耗

### #1

关键词：LT9721 PowerControl

类型：BugFix

问题描述：为了降低功耗，在未连接眼镜的情况下，需要通过MOSFET关闭LT9721 IC的芯片，以降低该场景下的功耗（降低越500mW）。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18781/ 

### #2

关键词：Poweroff Panic

类型：BugFix

问题描述：测试发现在关机过程中，偶现关机后无法再次启动，分析日志后发现该问题是由于关机是断电操作时机不合理导致系统未完全关闭，仍在执行导致Kernel Panic（Watchdog Error）。

相关日志：

```c
[   72.105594@0] HARDLOCKUP____ERR.CPU[1] <irqen:0 isren1>START
[   72.110552@0] 
[   72.110552@0] dump_cpu[0] irq:  5 preempt:10001  swapper/0
[   72.117665@0] IRQ arch_timer_handler_phys, 10000
[   72.122250@0] Task dump for CPU 0:
[   72.125631@0] swapper/0       R  running task        0     0      0 0x00000000
[   72.132824@0] Call trace:
[   72.135429@0] [ffffffc07472ac40+ 112][<ffffff800908b390>] dump_backtrace+0x0/0x250
[   72.142966@0] [ffffffc07472acb0+  32][<ffffff800908b664>] show_stack+0x24/0x30
[   72.150161@0] [ffffffc07472acd0+  32][<ffffff80090d9df0>] sched_show_task+0x140/0x190
[   72.157959@0] [ffffffc07472acf0+  32][<ffffff80090dca70>] dump_cpu_task+0x48/0x58x
[   72.165415@0] [ffffffc07472ad10+ 112][<ffffff8009a3c614>] pr_lockup_info+0x174/0x220
[   72.173127@0] [ffffffc07472ad80+  48][<ffffff8009165168>] watchdog_check_hardlockup_oth8
[   72.182571@0] [ffffffc07472adb0+ 112][<ffffff80091642a4>] watchdog_timer_fn+0xcc/0x320
[   72.190461@0] [ffffffc07472ae20+ 128][<ffffff8009124bc8>] __hrtimer_run_queues+0xd0/0x38
[   72.198605@0] [ffffffc07472aea0+  96][<ffffff8009125574>] hrtimer_interrupt+0xb4/0x1e8
[   72.206492@0] [ffffffc07472af00+  32][<ffffff80097b247c>] arch_timer_handler_phys+0x3c/0
[   72.214813@0] [ffffffc07472af20+  96][<ffffff8009112288>] handle_percpu_devid_irq+0xc0/0
[   72.223220@0] [ffffffc07472af80+  32][<ffffff800910c684>] generic_handle_irq+0x34/0x50
[   72.231105@0] [ffffffc07472afa0+  96][<ffffff800910cd94>] __handle_domain_irq+0x8c/0x100
[   72.239165@0] [ffffffc07472b000+  64][<ffffff80090815f4>] gic_handle_irq+0x5c/0xb0
[   72.246704@0] Exception stack(0xffffff800a7d3dc0 to 0xffffff800a7d3ef0)
[   72.253292@0] 3dc0: 0000000000000000 ffffff800aaebc90 0000000000000000 0000000000000001
[   72.261264@0] 3de0: 000000406a372000 00ffffffffffffff 0000000003762f33 0000000000000000
[   72.269238@0] 3e00: ffffff800a7e92f0 ffffff800a7d3e60 00000000000009d0 00000000000000ff
[   72.277211@0] 3e20: 0000000000000400 0000000000000000 ffffff808a996127 ffffffffffffffff
[   72.285184@0] 3e40: ffffff8009276d68 0000000000000000 0000000000000001 ffffff800a7d93a4
[   72.293158@0] 3e60: 0000000000000000 0000000000000001 0000000000000000 ffffff800a7e88c0
[   72.301131@0] 3e80: 0000000000000000 ffffff800a3bf748 ffffff800a1fb520 ffffff800a7d9000
[   72.309104@0] 3ea0: ffffff800a7d9000 ffffff800a7d3ef0 ffffff8009085de4 ffffff800a7d3ef0
[   72.317078@0] 3ec0: ffffff8009085de8 0000000000c00149 0000000000000000 0000000000000000
[   72.325050@0] 3ee0: ffffffffffffffff 0000000000000000
[   72.330079@0] [ffffffc07472b040+   0][<ffffff8009083144>] el1_irq+0x104/0x178
[   72.337185@0] [ffffff800a7d3ef0+  32][<ffffff8009085de8>] arch_cpu_idle+0x30/0x190
[   72.344728@0] [ffffff800a7d3f10+  16][<ffffff8009d75b18>] default_idle_call+0x20/0x3c
[   72.352527@0] [ffffff800a7d3f20+  96][<ffffff80090f8024>] cpu_startup_entry+0x1a4/0x208
[   72.360499@0] [ffffff800a7d3f80+  32][<ffffff8009d6dd94>] rest_init+0x84/0x90
[   72.367608@0] [ffffff800a7d3fa0+  80][<ffffff800a2d0b78>] start_kernel+0x388/0x39c
[   72.375145@0] [ffffff800a7d3ff0+   0][<ffffff800a2d0200>] __primary_switched+0x7c/0x90
[   72.383030@0] 
[   72.383030@0] HARDLOCKUP____ERR.END
[   72.383030@0] 
[   72.389790@0] Watchdog detected hard LOCKUP on cpu 1[   72.394563@0] ------------[ cut -
[   72.399327@0] WARNING: CPU: 0 PID: 0 at /home/zxy/project/compal-dev/common/kernel/watc8
[   72.412670@0] Modules linked in: cywdhd(O) galcore(O) mali_kbase(O) vpu(O) encoder(O) a)
[   72.446296@0] 
[   72.447947@0] CPU: 0 PID: 0 Comm: swapper/0 Tainted: G           O    4.9.113 #10
[   72.455396@0] Hardware name: Amlogic (DT)
[   72.459383@0] task: ffffff800a7e88c0 task.stack: ffffff800a7d0000
[   72.465451@0] PC is at watchdog_check_hardlockup_other_cpu+0x114/0x138
[   72.471951@0] LR is at watchdog_check_hardlockup_other_cpu+0x114/0x138
[   72.478450@0] pc : [<ffffff8009165184>] lr : [<ffffff8009165184>] pstate: 604001c9
[   72.485989@0] sp : ffffffc07472ad80
[   72.489456@0] x29: ffffffc07472ad80 x28: ffffff800a7e88c0 
[   72.494916@0] x27: ffffff800a7da000 x26: ffffff800aa3d398 
[   72.500376@0] x25: ffffff800a7da478 x24: 0000000000000000 
[   72.505836@0] x23: ffffffc07472e6d8 x22: 0000000000000001 
[   72.511296@0] x21: 0000000000000001 x20: ffffff800a7db548 
[   72.516756@0] x19: ffffff800a3bc739 x18: 0000000000000001 
[   72.522216@0] x17: ffffff800a7d3ef0 x16: ffffff8009276d68 
[   72.527676@0] x15: ffffffffffffffff x14: ffffff808a996127 
[   72.533136@0] x13: ffffff800a996137 x12: 444e452e5252455f 
[   72.538596@0] x11: ffffffc07472ab40 x10: 0000000005f5e0ff 
[   72.544056@0] x9 : 00000000ffffffd0 x8 : 000000000000086b 
[   72.549516@0] x7 : ffffff800a840c70 x6 : 0000000000000037 
[   72.554976@0] x5 : 0000000000000000 x4 : 0000000000000030 
[   72.560436@0] x3 : ffffff8008149ae8 x2 : 0000000000000000 
[   72.565896@0] x1 : 0000000000010001 x0 : 0000000000000026 
[   72.571357@0] 
[   72.571357@0] SP: 0xffffffc07472ad00:
[   72.576643@0] ad00  00000001 00000000 7472e6d8 ffffffc0 00000000 00000000 0a7da478 ffff0
[   72.584962@0] ad20  0aa3d398 ffffff80 0a7da000 ffffff80 0a7e88c0 ffffff80 7472ad80 ffff0
[   72.593282@0] ad40  09165184 ffffff80 7472ad80 ffffffc0 09165184 ffffff80 604001c9 00000
[   72.601603@0] ad60  00000000 00000000 00000000 00000000 ffffffff 0000007f 00000000 00000
[   72.609922@0] ad80  7472adb0 ffffffc0 091642a4 ffffff80 0a1fb520 ffffff80 0a3bc6a8 ffff0
[   72.618242@0] ada0  0a3bc000 ffffff80 0aa3d000 ffffff80 7472ae20 ffffffc0 09124bc8 ffff0
[   72.626562@0] adc0  7472e6d8 ffffffc0 7472e140 ffffffc0 7472e180 ffffffc0 091641d8 ffff0
[   72.634882@0] ade0  0a7d92f8 ffffff80 0a7d9000 ffffff80 0a3b5000 ffffff80 00000001 00000
[   72.643205@0] 
[   72.643205@0] X11: 0xffffffc07472aac0:
[   72.648576@0] aac0  604001c9 00000000 0000003d 00000000 7472ab10 ffffffc0 09d76188 ffff0
[   72.656896@0] aae0  7472aaf0 ffffffc0 09459b6c ffffff80 7472ab30 ffffffc0 0908b82c ffff0
[   72.665216@0] ab00  7472ac50 ffffffc0 7472ac50 ffffffc0 0908b890 ffffff80 0a995be8 ffff0
[   72.673536@0] ab20  7472ab40 ffffffc0 09d76194 ffffff80 7472ab50 ffffffc0 0908b8d0 ffff0
[   72.681856@0] ab40  7472ac50 ffffffc0 0910a948 ffffff80 7472ab70 ffffffc0 090849d8 ffff0
[   72.690176@0] ab60  f2000800 00000000 09084978 ffffff80 7472aba0 ffffffc0 09081534 ffff0
[   72.698496@0] ab80  7472ac50 ffffffc0 f2000800 00000000 ef1099b8 00000000 00000026 00000
[   72.706816@0] aba0  7472ad80 ffffffc0 09082fbc ffffff80 0a3bc739 ffffff80 ffffffff 0000f
[   72.715139@0] 
[   72.715139@0] X23: 0xffffffc07472e658:
[   72.720510@0] e658  00000000 00000000 541d0000 ffffffc0 00150015 00000001 7472e670 ffff0
[   72.728829@0] e678  7472e670 ffffffc0 7472e680 ffffffc0 7472e680 ffffffc0 09157348 ffff0
[   72.737149@0] e698  20003d08 ffffff80 20003c78 ffffff80 00000000 00000000 00000012 00000
[   72.745469@0] e6b8  00000011 00000000 541d1b80 ffffffc0 00000000 00000000 00000000 00000
[   72.753789@0] e6d8  7472e6d8 ffffffc0 00000000 00000000 00000000 00000000 c7596000 00000
[   72.762109@0] e6f8  c7596000 00000010 091641d8 ffffff80 7472e180 ffffffc0 00000000 0000c
[   72.770429@0] e718  091649e0 ffffff80 63746177 676f6468 0000302f 00000000 0000000c 00000
[   72.778749@0] e738  00000000 00000000 00000000 00000000 00000000 00000000 7472e750 ffff0
[   72.787071@0] 
[   72.787071@0] X29: 0xffffffc07472ad00:
[   72.792443@0] ad00  00000001 00000000 7472e6d8 ffffffc0 00000000 00000000 0a7da478 ffff0
[   72.800762@0] ad20  0aa3d398 ffffff80 0a7da000 ffffff80 0a7e88c0 ffffff80 7472ad80 ffff0
[   72.809082@0] ad40  09165184 ffffff80 7472ad80 ffffffc0 09165184 ffffff80 604001c9 00000
[   72.817402@0] ad60  00000000 00000000 00000000 00000000 ffffffff 0000007f 00000000 00000
[   72.825722@0] ad80  7472adb0 ffffffc0 091642a4 ffffff80 0a1fb520 ffffff80 0a3bc6a8 ffff0
[   72.834042@0] ada0  0a3bc000 ffffff80 0aa3d000 ffffff80 7472ae20 ffffffc0 09124bc8 ffff0
[   72.842362@0] adc0  7472e6d8 ffffffc0 7472e140 ffffffc0 7472e180 ffffffc0 091641d8 ffff0
[   72.850683@0] ade0  0a7d92f8 ffffff80 0a7d9000 ffffff80 0a3b5000 ffffff80 00000001 00000
[   72.859002@0] 
[   72.860650@0] ---[ end trace 9995486328ea65e9 ]---
[   72.865416@0] Call trace:
[   72.868017@0] Exception stack(0xffffffc07472abb0 to 0xffffffc07472ace0)
[   72.874604@0] aba0:                                   ffffff800a3bc739 0000007fffffffff
[   72.882578@0] abc0: ffffffc074735a90 ffffff8009165184 ffffff800a7d92f8 000000000a802608
[   72.890551@0] abe0: ffffffc07472ac80 ffffff800910b138 ffffffc07472ace0 ffffff800a0cd4e0
[   72.898524@0] ac00: 0000000000000001 0000000000000001 ffffffc07472e6d8 0000000000000000
[   72.906498@0] ac20: ffffff800a7da478 ffffff800aa3d398 ffffff800a7da000 ffffff800a7e88c0
[   72.914471@0] ac40: ffffffc07472acd0 0000000000000000 0000000000000026 0000000000010001
[   72.922444@0] ac60: 0000000000000000 ffffff8008149ae8 0000000000000030 0000000000000000
[   72.930418@0] ac80: 0000000000000037 ffffff800a840c70 000000000000086b 00000000ffffffd0
[   72.938391@0] aca0: 0000000005f5e0ff ffffffc07472ab40 444e452e5252455f ffffff800a996137
[   72.946364@0] acc0: ffffff808a996127 ffffffffffffffff ffffff8009276d68 ffffff800a7d3ef0
[   72.954339@0] [ffffffc07472ad80+  48][<ffffff8009165184>] watchdog_check_hardlockup_oth8
[   72.963872@0] [ffffffc07472adb0+ 112][<ffffff80091642a4>] watchdog_timer_fn+0xcc/0x320
[   72.971760@0] [ffffffc07472ae20+ 128][<ffffff8009124bc8>] __hrtimer_run_queues+0xd0/0x38
[   72.979905@0] [ffffffc07472aea0+  96][<ffffff8009125574>] hrtimer_interrupt+0xb4/0x1e8
[   72.987793@0] [ffffffc07472af00+  32][<ffffff80097b247c>] arch_timer_handler_phys+0x3c/0
[   72.996112@0] [ffffffc07472af20+  96][<ffffff8009112288>] handle_percpu_devid_irq+0xc0/0
[   73.004520@0] [ffffffc07472af80+  32][<ffffff800910c684>] generic_handle_irq+0x34/0x50
[   73.012405@0] [ffffffc07472afa0+  96][<ffffff800910cd94>] __handle_domain_irq+0x8c/0x100
[   73.020465@0] [ffffffc07472b000+  64][<ffffff80090815f4>] gic_handle_irq+0x5c/0xb0
[   73.028004@0] Exception stack(0xffffff800a7d3dc0 to 0xffffff800a7d3ef0)
[   73.034591@0] 3dc0: 0000000000000000 ffffff800aaebc90 0000000000000000 0000000000000001
[   73.042564@0] 3de0: 000000406a372000 00ffffffffffffff 0000000003762f33 0000000000000000
[   73.050538@0] 3e00: ffffff800a7e92f0 ffffff800a7d3e60 00000000000009d0 00000000000000ff
[   73.058511@0] 3e20: 0000000000000400 0000000000000000 ffffff808a996127 ffffffffffffffff
[   73.066484@0] 3e40: ffffff8009276d68 0000000000000000 0000000000000001 ffffff800a7d93a4
[   73.074458@0] 3e60: 0000000000000000 0000000000000001 0000000000000000 ffffff800a7e88c0
[   73.082431@0] 3e80: 0000000000000000 ffffff800a3bf748 ffffff800a1fb520 ffffff800a7d9000
[   73.090404@0] 3ea0: ffffff800a7d9000 ffffff800a7d3ef0 ffffff8009085de4 ffffff800a7d3ef0
[   73.098378@0] 3ec0: ffffff8009085de8 0000000000c00149 0000000000000000 0000000000000000
[   73.106350@0] 3ee0: ffffffffffffffff 0000000000000000
[   73.111378@0] [ffffffc07472b040+   0][<ffffff8009083144>] el1_irq+0x104/0x178
[   73.118485@0] [ffffff800a7d3ef0+  32][<ffffff8009085de8>] arch_cpu_idle+0x30/0x190
[   73.126026@0] [ffffff800a7d3f10+  16][<ffffff8009d75b18>] default_idle_call+0x20/0x3c
[   73.133826@0] [ffffff800a7d3f20+  96][<ffffff80090f8024>] cpu_startup_entry+0x1a4/0x208
[   73.141798@0] [ffffff800a7d3f80+  32][<ffffff8009d6dd94>] rest_init+0x84/0x90
[   73.148906@0] [ffffff800a7d3fa0+  80][<ffffff800a2d0b78>] start_kernel+0x388/0x39c
[   73.156445@0] [ffffff800a7d3ff0+   0][<ffffff800a2d0200>] __primary_switched+0x7c/0x90
```



相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18781/ 

禅道编号：25014



## 休眠唤醒

### #1

关键词：休眠唤醒

类型：Feature

问题描述：为了保证在休眠场景下，连接眼镜/充电器能够唤醒系统，在Uboot中配置了GPIOAO_2(CC Logic Chip FUSB302 Interrupt PIN)作为中断唤醒的唤醒源以支持休眠唤醒。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/18883/ 



## 其他

### #1

关键词：FrameBuffer 内存越界 KernelPanic

类型：BugFix

问题描述：Framebuffer配置的开机动画Logo内存地址超过了内存最大地址，形成了地址越界，导致系统重启。

解决方案：修改开机动画Logo加载的内存地址，使其小于内存最大地址。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16297/

### #2

关键词：USB Extcon

类型：BugFix

问题描述：USB PHY驱动中判断逻辑错误，导致无法正常识别眼镜设备。

解决方案：修正USB PHY驱动中的判断逻辑。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/16616/



### #3

关键词：Glass Fusb302 CCLogic

类型：BugFix

问题描述：眼镜连上Dock后开机，眼镜无法正常显示。

解决方案：该问题由Soc在关机状态下未检测到眼镜插入信号导致，通过在系统启动后强制进行一次连接检测以确认当前的链接状态以修复该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17174/



### #4

关键词：恢复出厂设置 关机充电模式

类型：BugFix

问题描述：通过设置中的恢复出厂设置进行恢复出厂设置会产生错误，排查发现传递androidboot.mode=normal会干扰恢复出厂设置。

解决方案：在恢复出厂设置时，不传递androidboot.mode=normal。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/17893/

### #5

关键词：Glass FUSB302 异常唤醒 无法唤醒 

类型：BugFix

问题描述：测试发现FUSB302上电后正常初始化，TypeC数据线一头通过连接器与FUSB302芯片相连，另一头空置，在该场景下，我们发现某些数据线会出现CC PIN上出现异常的信号，同时不断触发FUSB302的中断信号。排查发现后该问题与与FUSB302中的一个寄存器值配置有关（TYPEC_RP_1A5 -> TYPEC_RP_USB）。修正该寄存器配置后，问题解决。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/21174/



### #6

关键词：Glass FUSB302 异常唤醒 无法唤醒 

类型：BugFix

问题描述：测试发现将Dock连接至PC充电后，再连接眼镜无法正常点亮，分析发现是连接过PC充电后，Charger无法正常进行Boost输出了，比对正常与异常的寄存器差异， 发现Charger在连接过PC后会自动设置为HIZ_MODE，导致无法对外输出电压，修改为拔出充电器后自动退出HIZ_MODE.问题修复。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/21122/



### #7

关键词：SOC VDDEE

类型：BugFix

问题描述：硬件测试发现系统启动后，VDDEE的电压低于预期设计的800mV，在进入休眠状态后退出，VDDEE电压达到预期值，排查发现该问题是由于更新DDR Training Timing时导入的参数中包含对于VDDEE配置表，其对于VDDEE的配置与Uboot中的配置不符。

解决方案：修正DDR Training Timing配置表中对于VDDEE参数的配置。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20607/



### #8

关键词：SOC I2C 时序限制

类型：BugFix

问题描述：硬件测试发现DOCK I2C2的时序不满足该总线上挂载的I2C从机芯片SN8F520212的要求。

解决方案：降低该I2C总线的时钟速率（400KHz -> 100KHz）,使其能够满足从机的时序要求。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20384/



### #9

关键词： Charger 灯效 

类型：BugFix

问题描述：使用过程中发现在拔出眼镜后，指示电量的4颗白色指示灯会异常的闪烁一次。

解决方案：排查后发现该问题是由于电源管理MCU判断是否处于充电状态的机制（MCU通过获取VBUS的电压和OTG_EN的电压判断）导致的，通过增加断开VBUS Boost输出 与 拉低OTG_EN之间的时间解决该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/20384/ / https://openai-corp.rokid.com/#/c/19640/



### #10

关键词： RTC

类型：BugFix

问题描述：使用过程中发现Dock在断开过电池后再启动时，会导致无法从RTC芯片中获取时间信息。

解决方案：该问题是由于PCF85063 RTC芯片在系统断电后会将芯片内的PCF85063_REG_SC_OS寄存器置位，在pcf85063_get_datetime函数中进行对PCF85063_REG_SC_OS进行了判断。通过去除PCF85063_REG_SC_OS判断逻辑修复该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/19889/



### #11

关键词： Uboot Recovery

类型：BugFix

问题描述：使用过程中发现通过adb reboot update 偶现会进入Recovery模式，导致DOCK无法正常使用。

解决方案：该问题是由于adb reboot update进入Uboot后在没有USB连接的情况下命中SD卡升级逻辑进入Recovery，通过移除Uboot中未连接USB的情况下进入SD卡升级的逻辑修复该问题。

相关提交Gerrit链接：https://openai-corp.rokid.com/#/c/19717/



# 专项问题分析报告

## #1

### 905D3内核 Panic分析

## 日志

```
<1>[ 1793.358994@1] Unable to handle kernel paging request at virtual address 4c8f400000874
<1>[ 1793.361184@1] pgd = ffffffc048f46000
<1>[ 1793.364730@1] [4c8f400000874] *pgd=0000000000000000, *pud=0000000000000000
<0>[ 1793.371580@1] Internal error: Oops: 96000004 [#1] PREEMPT SMP
<4>[ 1793.377299@1] Modules linked in: cywdhd(O) galcore(O) mali_kbase(O) vpu(O) encoder(O) amvdec_avs2(O) amvdec_vp9(O) amvdec_vc1(O) amvdec_real(O) amvdec_mmpeg4(O) amvdec_mpeg4(O) amvdec_mmpeg12(O) amvdec_mpeg12(O) amvdec_mmjpeg(O) amvdec_mjpeg(O) amvdec_h265(O) amvdec_h264mvc(O) amvdec_mh264(O) amvdec_h264(O) amvdec_avs(O) stream_input(O) decoder_common(O) firmware(O) media_clock(O)
<4>[ 1793.410927@1] CPU: 1 PID: 3304 Comm: HwBinder:3127_1 Tainted: G        W  O    4.9.113 #1
<4>[ 1793.419070@1] Hardware name: Amlogic (DT)
<4>[ 1793.423058@1] task: ffffffc049f944c0 task.stack: ffffff80204d0000
<4>[ 1793.429130@1] PC is at rb_insert_color+0x48/0x1a0
<4>[ 1793.433807@1] LR is at sync_pt_create+0x118/0x268
<4>[ 1793.438484@1] pc : [<ffffff8009461cd8>] lr : [<ffffff800955d728>] pstate: 204001c9
<4>[ 1793.446022@1] sp : ffffff80204d3a80
<4>[ 1793.449489@1] x29: ffffff80204d3a80 x28: ffffffc049f944c0 
<4>[ 1793.454949@1] x27: 0000000000000000 x26: ffffff800aa96558 
<4>[ 1793.460409@1] x25: 0000000000000000 x24: ffffffc04aaf7600 
<4>[ 1793.465869@1] x23: ffffffc04a867540 x22: ffffffc04aaf7670 
<4>[ 1793.471329@1] x21: ffffffc04aaf7660 x20: ffffffc04a867550 
<4>[ 1793.476789@1] x19: ffffffc04aaf7600 x18: 0000000000000000 
<4>[ 1793.482249@1] x17: 0000000000000000 x16: ffffff8009276d68 
<4>[ 1793.487709@1] x15: 0000000000000000 x14: 8001030000000500 
<4>[ 1793.493169@1] x13: 00001400000002d0 x12: 0000050000000000 
<4>[ 1793.498629@1] x11: 0000000000000001 x10: 000000010000001e 
<4>[ 1793.504089@1] x9 : 0000000000000000 x8 : ffffffc04aaf76c0 
<4>[ 1793.509549@1] x7 : ffffffbf001e78c0 x6 : ffffffc053c297a0 
<4>[ 1793.515009@1] x5 : ffffffbf001e78c1 x4 : ffffffc053c29908 
<4>[ 1793.520469@1] x3 : 0104c8f400000874 x2 : ffffffbf001e78c0 
<4>[ 1793.525929@1] x1 : ffffffc04a867538 x0 : ffffffc04aaf7670 
<4>[ 1793.531390@1] 
<4>[ 1793.531390@1] X0: 0xffffffc04aaf75f0:
<4>[ 1793.536675@1] 75f0  00000000 00000000 00000000 00000000 00000001 00000000 09ddd960 ffffff80

<4>[ 1794.849077@1] [ffffff80204d3a80+  64][<ffffff8009461cd8>] rb_insert_color+0x48/0x1a0
<4>[ 1794.856788@1] [ffffff80204d3ac0+  48][<ffffff800955d8b8>] aml_sync_create_fence+0x40/0xd8
<4>[ 1794.864934@1] [ffffff80204d3af0+  32][<ffffff80098a01bc>] osd_timeline_create_fence+0x2c/0x68
<4>[ 1794.873425@1] [ffffff80204d3b10+  64][<ffffff80098a031c>] out_fence_create+0x124/0x198
<4>[ 1794.881312@1] [ffffff80204d3b50+ 112][<ffffff80098a1154>] sync_render_layers_fence+0x174/0x208
<4>[ 1794.889893@1] [ffffff80204d3bc0+  64][<ffffff80098a45d4>] osd_sync_request_render+0xcc/0x140
<4>[ 1794.898299@1] [ffffff80204d3c00+ 448][<ffffff80098b34a4>] osd_ioctl+0xd24/0x11f8
<4>[ 1794.905666@1] [ffffff80204d3dc0+  48][<ffffff80098b39b8>] osd_compat_ioctl+0x40/0x78
<4>[ 1794.913381@1] [ffffff80204d3df0+ 144][<ffffff80094c63cc>] fb_compat_ioctl+0x63c/0x9b8
<4>[ 1794.921181@1] [ffffff80204d3e80+   0][<ffffff8009276e2c>] compat_SyS_ioctl+0xc4/0xf70
<4>[ 1794.928980@1] [0000000000000000+   0][<ffffff80090838c0>] el0_svc_naked+0x34/0x38
<0>[ 1794.936432@1] Code: f9400043 927ef463 f9000043 b40008a3 (f9400062) 
<3>[ 1794.942727@1] sched: RT throttling activated for rt_rq ffffffc074750ba0 (cpu 1)
<3>[ 1794.942727@1] potential CPU hogs:
<3>[ 1794.942727@1] 	FastCapture (11784)
<3>[ 1794.942727@1] 	sfEventThread (3183)
<3>[ 1794.942727@1] 	HwBinder:3127_1 (3304)

```

## 现象分析

<1>[ 1793.358994@1] Unable to handle kernel paging request at virtual address 4c8f400000874
<0>[ 1793.371580@1] Internal error: Oops: 96000004 [#1] PREEMPT SMP

以上两行内核日志说明内核发生了空指针导致的内核Panic，进而触发了内核重启。

## 问题分析

通过gdb工具来定位实际导致内核Panic的代码：

<4>[ 1793.429130@1] PC is at rb_insert_color+0x48/0x1a0

shell>/opt/gcc-linaro-6.3.1-2017.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-gdb vmlinux
gdb>list*rb_insert_color+0x48

可以通过gdb返回的信息发现导致内核Panic的代码为：

```
(gdb) list*rb_insert_color+0x48
0xffffff8009461cd8 is in rb_insert_color (/home/zxy/project/compal-dev/common/lib/rbtree.c:113).
108			 * want a red root or two consecutive red nodes.
109			 */
110			if (!parent) {
111				rb_set_parent_color(node, NULL, RB_BLACK);
112				break;
113			} else if (rb_is_black(parent))
114				break;
115
116			gparent = rb_red_parent(parent);
117
```

确认对应的函数为 rb_insert_color.
再通过下列命令获取对应汇编代码：
gdb>disassemble rb_insert_color

```
Dump of assembler code for function rb_insert_color:
   0xffffff8009461c90 <+0>:	ldr	x3, [x0]
   0xffffff8009461c94 <+4>:	cbz	x3, 0xffffff8009461dec <rb_insert_color+348>
   0xffffff8009461c98 <+8>:	ldr	x2, [x3]
   0xffffff8009461c9c <+12>:	tbnz	w2, #0, 0xffffff8009461e20 <rb_insert_color+400>
   0xffffff8009461ca0 <+16>:	ldr	x4, [x2,#8]
   0xffffff8009461ca4 <+20>:	orr	x5, x2, #0x1
   0xffffff8009461ca8 <+24>:	mov	x7, x2
   0xffffff8009461cac <+28>:	cmp	x4, x3
   0xffffff8009461cb0 <+32>:	b.eq	0xffffff8009461d38 <rb_insert_color+168>
   0xffffff8009461cb4 <+36>:	cbz	x4, 0xffffff8009461ce8 <rb_insert_color+88>
   0xffffff8009461cb8 <+40>:	ldr	x6, [x4]
   0xffffff8009461cbc <+44>:	tbnz	w6, #0, 0xffffff8009461ce8 <rb_insert_color+88>
   0xffffff8009461cc0 <+48>:	str	x5, [x4]
   0xffffff8009461cc4 <+52>:	str	x5, [x3]
   0xffffff8009461cc8 <+56>:	ldr	x3, [x2]
   0xffffff8009461ccc <+60>:	and	x3, x3, #0xfffffffffffffffc
   0xffffff8009461cd0 <+64>:	str	x3, [x2]
   0xffffff8009461cd4 <+68>:	cbz	x3, 0xffffff8009461de8 <rb_insert_color+344>
   0xffffff8009461cd8 <+72>:	ldr	x2, [x3]
   0xffffff8009461cdc <+76>:	mov	x0, x7
   0xffffff8009461ce0 <+80>:	tbz	w2, #0, 0xffffff8009461ca0 <rb_insert_color+16>
   0xffffff8009461ce4 <+84>:	ret
   0xffffff8009461ce8 <+88>:	ldr	x4, [x3,#8]
   0xffffff8009461cec <+92>:	cmp	x4, x0
   0xffffff8009461cf0 <+96>:	b.eq	0xffffff8009461db0 <rb_insert_color+288>
   0xffffff8009461cf4 <+100>:	mov	x0, x3
   0xffffff8009461cf8 <+104>:	str	x4, [x2,#16]
   0xffffff8009461cfc <+108>:	str	x2, [x3,#8]
   0xffffff8009461d00 <+112>:	cbz	x4, 0xffffff8009461d0c <rb_insert_color+124>
   0xffffff8009461d04 <+116>:	orr	x5, x2, #0x1
   0xffffff8009461d08 <+120>:	str	x5, [x4]
   0xffffff8009461d0c <+124>:	ldr	x4, [x2]
   0xffffff8009461d10 <+128>:	str	x4, [x3]
   0xffffff8009461d14 <+132>:	str	x0, [x2]
   0xffffff8009461d18 <+136>:	ands	x4, x4, #0xfffffffffffffffc
   0xffffff8009461d1c <+140>:	b.eq	0xffffff8009461de0 <rb_insert_color+336>
   0xffffff8009461d20 <+144>:	ldr	x1, [x4,#16]
   0xffffff8009461d24 <+148>:	cmp	x2, x1
   0xffffff8009461d28 <+152>:	b.eq	0xffffff8009461dd8 <rb_insert_color+328>
   0xffffff8009461d2c <+156>:	str	x0, [x4,#8]
   0xffffff8009461d30 <+160>:	ret
   0xffffff8009461d34 <+164>:	nop
   0xffffff8009461d38 <+168>:	ldr	x5, [x2,#16]
   0xffffff8009461d3c <+172>:	orr	x6, x2, #0x1
   0xffffff8009461d40 <+176>:	cbz	x5, 0xffffff8009461d4c <rb_insert_color+188>
   0xffffff8009461d44 <+180>:	ldr	x8, [x5]
   0xffffff8009461d48 <+184>:	tbz	w8, #0, 0xffffff8009461d98 <rb_insert_color+264>
   0xffffff8009461d4c <+188>:	ldr	x3, [x4,#16]
   0xffffff8009461d50 <+192>:	cmp	x3, x0
   0xffffff8009461d54 <+196>:	b.eq	0xffffff8009461df8 <rb_insert_color+360>
   0xffffff8009461d58 <+200>:	mov	x0, x4
   0xffffff8009461d5c <+204>:	str	x3, [x2,#8]
   0xffffff8009461d60 <+208>:	str	x2, [x4,#16]
   0xffffff8009461d64 <+212>:	cbz	x3, 0xffffff8009461d70 <rb_insert_color+224>
   0xffffff8009461d68 <+216>:	orr	x5, x2, #0x1
   0xffffff8009461d6c <+220>:	str	x5, [x3]
   0xffffff8009461d70 <+224>:	ldr	x3, [x2]
   0xffffff8009461d74 <+228>:	str	x3, [x4]
   0xffffff8009461d78 <+232>:	str	x0, [x2]
```

通过内核打印的日志：
<4>[ 1793.438484@1] 

我们将PC= ffffff8009461cd8 所对应的汇编代码找到（汇编代码地址后的偏移<+72>与 rb_insert_color+0x48 中的+0x48也相等）：
0xffffff8009461cd4 <+68>:	cbz	x3, 0xffffff8009461de8 <rb_insert_color+344>
0xffffff8009461cd8 <+72>:	ldr	x2, [x3]

查阅aarch64指令集手册可以得知:
LDR     r8,[r10]            ; loads R8 from the address in R10.

<4>[ 1793.520469@1] x3 : 0104c8f400000874 x2 : ffffffbf001e78c0 
<4>[ 1793.525929@1] x1 : ffffffc04a867538 x0 : ffffffc04aaf7670 

分析到这里，我们可以初步推测 X3寄存器所指向的地址0104c8f400000874 可能是非法的，对该地址进行取值，触发了内核Panic.

```
static __always_inline void
__rb_insert(struct rb_node *node, struct rb_root *root,
      void (*augment_rotate)(struct rb_node *old, struct rb_node *new))
{
  struct rb_node *parent = rb_red_parent(node), *gparent, *tmp;

  while (true) {
    /*
     * Loop invariant: node is red
     *
     * If there is a black parent, we are done.
     * Otherwise, take some corrective action as we don't
     * want a red root or two consecutive red nodes.
     */
    if (!parent) {
      rb_set_parent_color(node, NULL, RB_BLACK);
      break;
    } else if (rb_is_black(parent))
      break;
```

回到代码中，我们可以推测调用__rb_insert时，传入的node结构体指针所指向的parent存在问题(node->__rb_parent_color)。
回溯代码：
static __always_inline void __rb_insert(struct rb_node *node, struct rb_root *root,void (*augment_rotate)(struct rb_node *old, struct rb_node *new)) <--
    void rb_insert_color(struct rb_node *node, struct rb_root *root) <--
      static struct sync_pt *sync_pt_create(struct sync_timeline *obj, unsigned int value)

```
static struct sync_pt *sync_pt_create(struct sync_timeline *obj,
        ┊ ┊ ┊ unsigned int value)
{
  struct sync_pt *pt;

  pt = kzalloc(sizeof(*pt), GFP_KERNEL);
  if (!pt)
    return NULL;

  sync_timeline_get(obj);
  fence_init(&pt->base, &timeline_fence_ops, &obj->lock,
    ┊ ┊obj->context, value);
  INIT_LIST_HEAD(&pt->link);

  spin_lock_irq(&obj->lock);
  if (!fence_is_signaled_locked(&pt->base)) {
    struct rb_node **p = &obj->pt_tree.rb_node;
    struct rb_node *parent = NULL;

    while (*p) {
      struct sync_pt *other;
      int cmp;

      parent = *p;
      other = rb_entry(parent, typeof(*pt), node);
      cmp = value - other->base.seqno;
      if (cmp > 0) {
        p = &parent->rb_right;
      } else if (cmp < 0) {
        p = &parent->rb_left;
      } else {
        if (fence_get_rcu(&other->base)) {
          fence_put(&pt->base);
          pt = other;
          goto unlock;
        }
        p = &parent->rb_left;
      }
    }
    rb_link_node(&pt->node, parent, p);
    rb_insert_color(&pt->node, &obj->pt_tree);

    parent = rb_next(&pt->node);
    list_add_tail(&pt->link,
      ┊ ┊ ┊ parent ? &rb_entry(parent, typeof(*pt), node)->link : &obj->pt_list);
  }
unlock:
  spin_unlock_irq(&obj->lock);

  return pt;
}
```

```
static inline void rb_link_node(struct rb_node *node, struct rb_node *parent,
        struct rb_node **rb_link)
{
  node->__rb_parent_color = (unsigned long)parent;
  node->rb_left = node->rb_right = NULL;

  *rb_link = node;
}
```

看到这里，我们可以初步推断，红黑树中的节点在其他某一处代码中，未被加锁就被释放了。

```
static void timeline_fence_release(struct fence *fence)
{
  struct sync_pt *pt = fence_to_sync_pt(fence);
  struct sync_timeline *parent = fence_parent(fence);

  if (!list_empty(&pt->link)) {
    unsigned long flags;

    spin_lock_irqsave(fence->lock, flags);
    if (!list_empty(&pt->link)) {
      list_del(&pt->link);
      rb_erase(&pt->node, &parent->pt_tree);
    }
    spin_unlock_irqrestore(fence->lock, flags);
  }

  sync_timeline_put(parent);
  fence_free(fence);
}
```

经过搜索，发现在timeline_fence_releas函数中，释放红黑树节点的时候，只对node节点进行了加锁,形成了Use-after-free，故导致了内核Panic.

​    

## #2

###  中断唤醒失败问题分析

## 现象描述

测试在进行连续插拔测试时，发现休眠情况下偶现（1/200）连接眼镜后无法唤醒系统，拆机测量后发现，在该异常情况下，CC逻辑芯片FUSB302的INTN（中断PIN）始终为高电平，初步判断问题为FUSB302硬件/软件异常导致无法产生中断唤醒系统。

## 问题排查

研发自行复现问题时发现，快速连接&断开眼镜时，极大概率能复现该问题，且复现该问题后，通过POWER键依然能够重新唤醒系统，，且唤醒后再休眠，该故障消失，故可以做如下判断：
1.核心系统仍处于正常工作状态（仍能被唤醒）
2.唤醒后的一些软件逻辑可导致问题消失。

分析代码后，可初步将问题指向FUSB302驱动中的中断相关逻辑。

## 代码分析

```
void fusb_irq_disable(struct fusb30x_chip *chip)
{
  unsigned long irqflags = 0;

  spin_lock_irqsave(&chip->irq_lock, irqflags);
  if (chip->enable_irq) {
    disable_irq_nosync(chip->gpio_int_irq);
    chip->enable_irq = 0;
  } else {
    dev_warn(chip->dev, "irq have already disabled\n");
  }
  spin_unlock_irqrestore(&chip->irq_lock, irqflags);
}

void fusb_irq_enable(struct fusb30x_chip *chip)
{
  unsigned long irqflags = 0;

  spin_lock_irqsave(&chip->irq_lock, irqflags);
  if (!chip->enable_irq) {
    enable_irq(chip->gpio_int_irq);
    chip->enable_irq = 1;
  }
  spin_unlock_irqrestore(&chip->irq_lock, irqflags);
}
```

分析代码后，确认是眼镜插拔过程中存在中断Enable/Disable逻辑错位导致的无法唤醒：即在插入眼镜后，Disable中断之后立即进入休眠，导致无法唤醒。

## 解决方法

```
 #include <linux/power_supply.h>
 #include <linux/workqueue.h>
 #include "fusb302.h"
+#include "../../../kernel/power/power.h"

+#define FUSB302_WAKELOCK "fusb302_irq_lock"
 #define FUSB302_MAX_REG                (FUSB_REG_FIFO + 50)
 #define FUSB_MS_TO_NS(x)       ((s64)x * 1000 * 1000)

@@ -3163,8 +3165,10 @@ static void state_machine_typec(struct fusb30x_chip *chip)
                return;
        }

-       if (!platform_get_device_irq_state(chip))
-               fusb_irq_enable(chip);
+       if (!platform_get_device_irq_state(chip)){
+       fusb_irq_enable(chip);
+  pm_wake_unlock(FUSB302_WAKELOCK);
+  }
        else
        queue_delayed_work(chip->fusb30x_wq, &chip->work,0);
 }
@@ -3257,6 +3261,7 @@ static void fusb302_work_func(struct work_struct *work)
        struct fusb30x_chip *chip;
   struct delayed_work* delay_work;
   delay_work = (struct delayed_work*)work;
+  pm_wake_lock(FUSB302_WAKELOCK);
   chip = container_of(delay_work, struct fusb30x_chip,work);
   state_machine_typec(chip);
 }
```

在Disable中断后，增加Wakelock休眠唤醒锁，避免在处理中断下半部的过程中进入休眠。

