---
id: Transplantation and development of EC20 on glass2
title: EC20在glass2上的移植开发
--- 

在 glass2 上，我这边主要负责移远 EC20 模组（型号：EC20CEFASG-256-SGNS）的移植工作，该模组支持 4G功能和GPS功能，下面从这两块进行介绍。  

## 4G功能简述：
EC20 是一款带分集接收功能的LTE-FDD/LTE-TDD/WCDMA/TD-SCDMA/CDMA/GSM无线通信模块，支持 LTE-FDD， LTE-TDD， DC-HSDPA， HSPA+， HSDPA， HSUPA， WCDMA， TD-SCDMA，CDMA， EDGE 和 GPRS 网络数据连接。下表所示是该模块支持的频段和功能：
![](../media/bsp/ec20_func.png)  
下表是该模块主要性能参数（仅供参考）：
![](../media/bsp/ec20_perform1.png)
![](../media/bsp/ec20_perform2.png)
![](../media/bsp/ec20_perform3.png)


## 4G移植和测试
### 4G移植
接手该模块时，系统已移植好该模块 4G 功能，因主控 USB 接口不够，EC20 模组和外部调试口共用一个 USB 接口，通过一个 USB switch 器件进行切换，如下图所示：
![](../media/bsp/ec20_switch.png)  
后期主要实现控制该 switch 器件进行切换的驱动工作，实现原理如下状态图所示，主要根据USB ID 和 VBUS pin进行判断切到4G还是外部调试口。驱动参考提交：<https://openai-corp.rokid.com/#/c/19643/>
![](../media/bsp/ec20_usb.png)  
驱动实现后，还需添加系统服务，以便系统开机时确认是否要打开4G功能。实现逻辑如下所示（code参考：<https://openai-corp.rokid.com/#/c/19883/>    <https://openai-corp.rokid.com/#/c/19884/>）：
![](../media/bsp/ec20_ltectl.png)

### 功能测试
移植好4G后，工厂那边需要对 4G 进行一些测试，测试项如下：
![](../media/bsp/ec20_ltetest.png)
根据如上测试项，我这边根据移远提供的《`Quectel_EC2x&EGxx_FTM_AT_Commands_Manual_V1.0_Preliminary_20190404`》文档实现了 FTM 工具，code提交参考：<https://openai-corp.rokid.com/#/c/19833>/ 

## GPS 简述
EC20 GPS 基于高通先进的 Gen8C Lite 技术， 集成了多星座 GNSS 接收机，支持 GPS/GLONASS/BeiDou/Galileo/QZSS 定位系统；支持标准 NMEA 0183 协议，默认通过 USB 接口输出 NMEA 语句（数据更新率： 1Hz）。
该模块的 GNSS 引擎默认关闭，可以通过 AT 命令打开。其他详细内容和GPS相关指令操作详见文档《`Quectel_EC20_R2.1_硬件设计手册_V1.1`》和《`Quectel_LTE_Standard_GNSS_应用指导_V1.0`》文档
该模块 GPS 默认配置是打开GPS、BeiDou、Galileo、GLONASS 四种导航系统的，可以通过如下命令进行配置（现模组固件不支持第7项）。更多其他配置参考 《`Quectel_LTE_Standard_GNSS_应用指导_V1.0`》文档。
![](../media/bsp/ec20_gnssat.png)  
如下是移远给出GPS 性能参数
![](../media/bsp/ec20_gpsperform1.png)
![](../media/bsp/ec20_gpsperform.png)


## GPS 移植和测试
### 参考提交：
<https://openai-corp.rokid.com/#/c/19531/>	在 amlogic-s905dx-p/platform/hardware/rokid/quectel_gps 目录下添加移远提供的 GPS 库和配置  
<https://openai-corp.rokid.com/#/c/19532/>	修改mk文件，添加导入移远提供的库 和 Android 原生 gnss 接口和服务  
<https://openai-corp.rokid.com/#/c/19533/>	在 amlogic-s905dx-p/platform/hardware/ril/quectel 目录下更新 ril 相关库和程序，支持GPS  

### 移植步骤：
1	从移远那边拿到 EC20 GPS 相关资料（GPS HAL 库以及移植文档，详见<Quectel_Android_GPS_Driver_V2.2.1_beta> 目录下资料）  
2	在 hardware/rokid/下新建 quectel_gps 目录，作为存放移远 EC20 HAL库位置  
注意 gps_cfg.inf 配置如下：  
```
#you can modify this config file to adapt your demand.
#the line start with ';' means not support
#the line start with '#' means use default config
NMEA_PORT_PATH=rild-nmea
;BAUD_RATE=115200
;RMC_HZ=1
;NMEA_PORT_PATH=/dev/ttyO1
;DATA_BITS=8
;STOP_BITS=1
;PARITY_TYPE=N
;FLOW_CONTROL=N
;LOG_LEVEL=LOG_DEBUG
```
3	移植移远提供的 HAL 库，并添加 Android 原生 GPS HAL 接口，修改如下：  
```
Twpra@dagobah:~/rokid_glass2_20200320/device/amlogic$ git diff .
diff --git a/cyclops/cyclops.mk b/cyclops/cyclops.mk
index ac03cd2..9857e9e 100644
--- a/cyclops/cyclops.mk
+++ b/cyclops/cyclops.mk
@@ -564,3 +564,17 @@ PRODUCT_COPY_FILES += \
     hardware/ril/quectel/ip-down:system/etc/ppp/ip-down \
     hardware/ril/quectel/ql-ril.conf:system/etc/ql-ril.conf \
     hardware/ril/quectel/libreference-ril.so:vendor/lib/libreference-ril.so
+
+#########################################################################
+# 
+#                               quectel gps
+# Info：wenzan.tu@rokid.com add quectel gps model at 20200327
+#########################################################################
+PRODUCT_COPY_FILES += \
+   hardware/rokid/quectel_gps/armeabi-v7a/gps.default.so:vendor/lib/hw/gps.default.so \
+   hardware/rokid/quectel_gps/gps_cfg.inf:vendor/etc/gps_cfg.inf
+
+PRODUCT_PACKAGES += \
+    android.hardware.gnss@1.0-impl \
+    android.hardware.gnss@1.0-service
```

4	由如下截图(device/amlogic/cyclops/BoardConfig.mk 内容)可知，项目manifest.xml 走的是 manifest_aosp.xml  
./cyclops.mk:192:TARGET_BUILD_GOOGLE_ATV:= true (通过分析可知该宏为 false)  
![](../media/bsp/ec20_config1.png)  
在 manifest_aosp.xml 添加如下配置：  
![](../media/bsp/ec20_diff.png)  

5	更新 hardware/ril/quectel 目录下的 ril 程序（移远提供），支持 GPS 功能  

### 功能测试：
移植完 GPS 后，可以安装 AndroiTSGPSTestFree.apk 或 gpstest.apk 应用进行测试，该应用可显示搜到多少颗卫星以及每颗卫星对应的 CNR 
除此之外，还可以通过FTM工具对 GPS 进行测试，工具使用方法参考文档《LTE及GPS FTM 测试说明-20200429》，工具soucecode 见 <https://openai-corp.rokid.com/#/c/19833/> 提交

## EC20 模组固件升级
EC20 模组支持在 DOCK 上进行升级，现最新固件版本为：`EC20CEFASGR08A03M2G`
最新固件以及在 DOCK 上的升级方法（readme.txt）、固件变更说明等资料，均在“EC20CEFASGR08A03M2G-固件升级包.zip”压缩包里