---
id: CloudService Architecture And Usage
title: CloudService云服务对接
--- 

## 

### 项目描述

CloudService以独立的系统服务app形式承接Rokid云服务基础网络交互功能，主要功能有

1. deviceToken获取、保持、结果分发；
2. 设备和用户绑定接口整套功能处理；
3. 用户登录接口处理；
4. 位置信息搜集上报；
5. 基础配置信息获取处理；

对外提供channelsdk aar插件（jcenter发布），用来调用CloudService提供的对外功能；


### 项目架构
#### 模块描述

* TimingManager

  定时、延迟任务管理器，主要处理deviceToken过期检测、定时请求；

* CommandReceiver

  指令接收器，接收过滤channelsdk过来的请求指令；

* NetStatusReceiver

  网络状态监听工具，监听网络可用变化；

* TokenManager

  deviceToken 管理，管理token检查、请求、结果存储、分发；

* BindUserDeviceManager

  用户设备绑定管理，接口请求、结果分发、error处理；

* ConfigInfoManager

  云端设备基本配置信息管理，接口请求、结果处理、error处理；

* PositionManager

  位置信息管理，位置信息收集、接口请求、结果处理、error处理；

* UserLoginManager

  用户登录管理，接口请求、结果分发、error处理；

* NetWorkManager

  网络请求工具模块，集成okhttp3、retrofit2、rxjava2等网络请求三方工具；


#### 架构图

![项目架构](./项目架构.jpg)

#### 服务策略

* 服务方式
  * 独立系统App内的独立Service形式；
  * 使用代权限广播来接收请求，分发结果；
* deviceToken请求触发
  * 所有请求统一交给TimerManager最终调用；
  * 网络连接时自动触发（开机必定进行网络检查）；
  * 定时一天后自动触发；
  * 用户通过ChannelManager请求触发；
* 回调处理逻辑
  * ChannelManager使用端单一处理，不支持同一请求多任务；
  * 广播处理，广播生命周期为ChannelManager附在Context为准；
  * ChannelManager中对请求自带超时处理，默认为15秒；

#### 主要逻辑图
##### deviceToken获取分发
![DeviceToken获取分发逻辑](./DeviceToken获取分发逻辑.jpg)

##### 用户设备绑定
1. 主流程与deviceToken类似，当deviceToken不存在时，会再进行一次deviceToken获取分发；

2. 如果结果返回deviceToken过期时，会进行一次deviceToken获取分发，如果本次deviceToken获取到，会进行一次bind请求，token未获取，直接返回error信息；

### 插件使用
#### channelsdk 引入及初始化

1. gradle 引入 sdk

   ```groovy
   implementation 'com.rokid.glass:channelsdk:1.0.8'
   ```

   目前channelsdk最新版本是 1.0.8

2. 工具类初始化

   ```kotlin
   // Activity 或 Service 中
   var channelManager: ChannelManager? = null
   // 
   override fun onCreate(savedInstanceState: Bundle?) {
           super.onCreate(savedInstanceState)
       channelManager = ChannelManager(this, false, false)
   }
   // 回收处理
   override fun onDestroy() {
       if (channelManager != null) {
           channelManager!!.onDestroy()
           channelManager = null
       }
       super.onDestroy()
   }
   ```

   ChannelManager初始化方法

   ```java
   /**
    * 初始化工具类
    *
    * @param context Context 上下文
    * @param useLogin 是否使用Login相关功能，默认为false，不使用
    * @param useBindDevice 是否使用用户设备绑定相关功能，默认为false，不使用
    */
   public ChannelManager(Context context, boolean useLogin, boolean useBindDevice)
   ```

   

#### 功能调用及结果处理

1. SaasToken使用 （最新Saas接口）

   ```java
   /**
    * 通过saas接口获取deviceToken
    *
    * @param onlyRemote 是否只用远程模式，默认为false，不使用；
    *                   远程模式：直接通知CloudService服务获取deviceToken
    *                   快速模式：先从系统数据库中查看是否有token，有则使用，没有则通知CloudService服务获取deviceToken
    * @param timeOut 超时时间，默认单位为秒
    * @param listener IDeviceTokenListener 回调监听
    */
   public void requestSaasToken(boolean onlyRemote, int timeOut, ITokenListener listener)
   
   /**
    * 通过saas接口获取deviceToken，默认超时时间为15秒
    *
    * @param onlyRemote 是否只用远程模式，默认为false，不使用；
    *                   远程模式：直接通知CloudService服务获取deviceToken
    *                   快速模式：先从系统数据库中查看是否有token，有则使用，没有则通知CloudService服务获取deviceToken
    * @param listener IDeviceTokenListener 回调监听
    */
   public void requestSaasToken(boolean onlyRemote, ITokenListener listener)
     
   /**
    * 获取Token 回调监听
    */
   public interface ITokenListener {
   
       /**
        * 请求成功
        *
        * @param token deviceToken
        */
       void onTokenSuccess(String token);
   
       /**
        * 请求失败
        *
        * @param error 错误信息
        */
       void onTokenFailed(String error);
   
       /**
        * 请求超时
        */
       void onTimeComplete();
   }
   ```
   
2. deviceToken使用 （老版本云端接口）

   ```java
   /**
    * 获取deviceToken，默认超时时间为15秒
    *
    * @param onlyRemote 是否只用远程模式，默认为false，不使用；
    *                   远程模式：直接通知CloudService服务获取deviceToken
    *                   快速模式：先从系统数据库中查看是否有token，有则使用，没有则通知CloudService服务获取deviceToken
    * @param listener ITokenListener 回调监听
    */
   public void requestToken(boolean onlyRemote, ITokenListener listener)
   
   /**
    * 获取deviceToken
    *
    * @param onlyRemote 是否只用远程模式，默认为false，不使用；
    *                   远程模式：直接通知CloudService服务获取deviceToken
    *                   快速模式：先从系统数据库中查看是否有token，有则使用，没有则通知CloudService服务获取deviceToken
    * @param timeOut 超时时间，默认单位为秒
    * @param listener ITokenListener 回调监听
    */
   public void requestToken(boolean onlyRemote, int timeOut, ITokenListener listener)
     
   /**
    * 获取Token 回调监听
    */
   public interface ITokenListener {
   
       /**
        * 请求成功
        *
        * @param token deviceToken
        */
       void onTokenSuccess(String token);
   
       /**
        * 请求失败
        *
        * @param error 错误信息
        */
       void onTokenFailed(String error);
   
       /**
        * 请求超时
        */
       void onTimeComplete();
   }
   ```

3. 用户设备绑定

   ```java
   /**
    * 用户设备绑定，默认超时时间为15秒
    *
    * @param bindToken 标志bind请求的token
    * @param userToken userToken，用户账户token
    * @param listener IBindDeviceListener 回调监听
    */
   public void bindDevice(String bindToken, String userToken, IBindDeviceListener listener)
   
   /**
    * 用户设备绑定
    *
    * @param bindToken 标志bind请求的token
    * @param userToken userToken，用户账户token
    * @param timeOut 超时时间，默认单位为秒
    * @param listener IBindDeviceListener 回调监听
    */
   public void bindDevice(String bindToken, String userToken, int timeOut, IBindDeviceListener listener)
   
   /**
    * 用户设备绑定请求回调监听
    */
   public interface IBindDeviceListener {
       /**
        * 绑定成功
        */
       void onBindSuccess();
       /**
        * 绑定失败
        *
        * @param error 错误信息
        */
       void onBindFailed(String error);
       /**
        * 请求超时
        */
       void onTimeComplete();
   }
   ```
   
4. 用户登录

   ```java
   /**
    * 用户账号登录，默认超时时间为15秒
    *
    * @param companyCode 公司名称或key
    * @param userName 账号名称
    * @param password 账号密码
    * @param listener IUserLoginListener 回调监听
    */
   public void loginUser(String companyCode, String userName, String password, IUserLoginListener listener)
   
   /**
    * 用户账号登录
    *
    * @param companyCode 公司名称或key
    * @param userName 账号名称
    * @param password 账号密码
    * @param timeOut 超时时间，默认单位为秒
    * @param listener IUserLoginListener 回调监听
    */
   public void loginUser(String companyCode, String userName, String password, int timeOut, IUserLoginListener listener)
   
   /**
    * 用户登录请求回调监听
    */
   public interface IUserLoginListener {
       /**
        * 请求成功
        *
        * @param token deviceToken
        */
       void onLoginSuccess(String token);
       /**
        * 请求失败
        *
        * @param error 错误信息
        */
       void onLoginFailed(String error);
       /**
        * 请求超时
        */
       void onTimeComplete();
   }
   ```





### 其他
#### 未完成服务

* 用户登录接口优化
* 设备配置信息接口使用
* 用户地理位置上报整套流程

#### 后续优化

* 系统启动时，deviceToken认证流程优化
* 上方接口deviceToken过期处理流程优化


