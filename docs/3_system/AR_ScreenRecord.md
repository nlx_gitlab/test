---
id: AR Projection
title: AR投屏
--- 

AR录屏框架设计
---------------------------------------
版本 | 时间 | Owner
---|---|----
v1.0 | 2020.6.9 | wenfeng.shi
v1.1 | 2020.6.11 | wenfeng.shi
v1.2 | 2020.6.11 | wenfeng.shi


名词 | 缩写
---|---
CameraWallpaper | CW
SurfaceFlinger | SF
VirtualDisplay | VD



### 总体原则

* 录屏应用为用户提供选项，是否允许叠加Camera预览，如用户设置不叠加，则在录屏过程中CW均不开启Camera预览；
* 在录屏应用允许叠加Camera预览的前提下，业务应用的每个页面均可控制是否叠加/不叠加 Camera预览，默认不叠加；
* 系统（CW和SF）判断是否要彻底关闭Camera预览；
* **已知缺陷**：当有多个录屏应用同时运行时，有一个要叠加，则全部叠加，所有录屏应用都退出，才关闭Camera预览；

### 框架设计
* 录屏应用、CW、SF、业务应用之间，通过广播通信；
	* ``android.intent.action.CAMERA_WALLPAPER_START`` --- 录屏应用开启时，如果用户允许叠加Camera预览，则发送此广播，通知CW用户允许叠加Camera预览，CW可以做好标记，Camera初始化等工作，但不要开启预览
	* ``android.intent.action.CAMERA_WALLPAPER_START_PREVIEW`` --- 业务应用发送，通知CW当前页面在录屏时需要叠加Camera预览
	* ``android.intent.action.CAMERA_WALLPAPER_STOP_PREVIEW`` --- 业务应用发送，当需要叠加预览的页面退出时，通知CW关闭Camera预览
	* ``android.intent.action.VIRTUAL_DISPLAY_CREATE`` --- SF发送，通知CW已创建VD，表示当前有录屏应用正在录屏
	* ``android.intent.action.VIRTUAL_DISPLAY_DESTROY`` ---SF发送，通知CW销毁VD，表示录屏应用退出

* 调用流程

```mermaid
sequenceDiagram
录屏应用A->>Wallpaper:CW_START
note right of Wallpaper:allow_preview默认值为false
Wallpaper->>Wallpaper:allow_preview=true
opt if(allow_preview==true && VD_Cound>0 && start_preview==true)
    Wallpaper->>Wallpaper:start camera preview
end
note right of Wallpaper:收到CW_START广播，allow_preview标志位置为true
录屏应用A->>SF:start record
SF->>Wallpaper:VD_CREATE
Wallpaper->>Wallpaper:VD_Count++(1)
opt if(allow_preview==true && VD_Cound>0 && start_preview==true)
    Wallpaper->>Wallpaper:start camera preview
end
note right of Wallpaper:VD_Count表示录屏应用个数的计数
业务应用->>Wallpaper:CW_START_PREVIEW
Wallpaper->>Wallpaper:start_preview=true
opt if(allow_preview==true && VD_Cound>0 && start_preview==true)
    Wallpaper->>Wallpaper:start camera preview
end
业务应用->>Wallpaper:CW_STOP_PREVIEW
Wallpaper->>Wallpaper:stop camera preview
Wallpaper->>Wallpaper:start_preview=false
录屏应用B->>SF:start record
SF->>Wallpaper:VD_CREATE
Wallpaper->>Wallpaper:VD_Count++(2)
录屏应用A->>SF:stop record
SF->>Wallpaper:VD_DESTROY
Wallpaper->>Wallpaper:VD_Count--(1)
录屏应用B->>SF:stop record
SF->>Wallpaper:VD_DESTROY
Wallpaper->>Wallpaper:VD_Count--(0)
note right of Wallpaper:VD_Count减到0，表示所有录屏应用都退出了，此时如预览未关闭，应关闭预览，销毁Camera实例，并将allow_preview置false
Wallpaper->>Wallpaper:destroy camera
Wallpaper->>Wallpaper:allow_preview=false
```

* 上图是两个录屏应用同时运行的过程。A应用允许叠加Camera预览，B应用不允许，此时以允许的app为准；
* 录屏应用、业务应用、SF均发送广播给CW，CW接收广播后进行状态标记及计数，根据状态和计数决定是否开启和关闭camera预览。