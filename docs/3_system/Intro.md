---
id: Overview Of System Technical Documents
title: 概述
---  

## 系统属性
### 已有属性

* 一代：ro.product.model=Rokid Glass
* 二代标准：ro.product.model=cyclops
* 二代红外：ro.product.model=cyclred
* 三代：ro.product.model=HiSTBAndroid Hi3796CV300

### 新增属性（二代1.1.1版本后，三代0.1.2版本后）
* 二代/三代：新增ro.rokid.product.model属性，值统一为“RG-”开头，已有属性如下
	* 二代标准：RG-cyclops
	* 二代红外：RG-cyclred
	* 三代标准：RG-titan
	* 头环标准：RG-crown
* App可根据ro.rokid.product.model属性值来判断是否为rokid glass设备，或者用于判断具体为哪个类型的设备。
* ro.product.model的值后续不会再改，维持当前值和厂商设置的值，若app使用该属性，需自行做好适配。


### 其他属性

* 默认Launcher：系统内存在多个Launcher并且未指定默认Launcher时，根据如下prop选取默认Launcher，不会弹窗给用户选择，设置内切换launcher时，需同时设置这两个属性值。
	* persist.boot.defaultlauncher=com.rokid.launcher3
	* persist.boot.defaultactivity=com.rokid.glass.launcher.RokidGlassLauncher

* alignment参数：供UI SDK使用，并且会根据接入的glass类型自适配，用户无需关心。
	* ro.rokid.alignment.left=376
	* ro.rokid.alignment.top=174
	* ro.rokid.alignment.right=938
	* ro.rokid.alignment.bottom=504

* glass相关参数：根据插入眼镜不同，以下值也会变动，一般来说，用户无需关心。
	* persist.rokid.glass.version= [4.0]      //glass固件版本
	* persist.rokid.glass.sn=[0701032022000198] //glass SN号
	* persist.rokid.glass.pcba=[0701052017000030]//glass pcba SN
	* persist.rokid.glass.typeid=[4B4FD8B3114248AEB678D323535116CD]//glass devicetypeID
	* persist.rokid.glass.optid=[B]//glass的光机版本