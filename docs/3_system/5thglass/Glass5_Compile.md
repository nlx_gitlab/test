---
id: Compilation Guide For Zhongyou Head Ring
title: 编译指南
--- 
 

---
## 中油头环：

* 下载：

```
$ repo init -u ssh://${user_name}@openai-corp.rokid.com:29418/amlogic-a311d-p/p-amlogic/platform/manifest.git -b rokid_glass5 -m rokid_glass5.xml --repo-url=ssh://${user_name}@openai-corp.rokid.com:29418/tools/repo --no-repo-verify
```

* 编译：

```
$ source build/envsetup.sh
$ lunch crown-userdebug
$ make otapackage -j32
```

* 烧写：

```
方法一：（脚本烧写）
    fastboot镜像地址：
        out/target/product/crown/crown-fastboot-flashall-*.zip
  	
    fastboot烧写方法：
        拷贝镜像到本地：scp ${user_name}@build-8:~/${your_code}/<out_img.zip>   ~/work/temp/
        手动解压zip
        $ 执行flash-all.sh
```
```
方法二：（PC工具烧写）
    PC工具镜像地址:
        out/target/product/crown/aml_upgrade_package.img
	
    PC工具烧写方法:
        1. 安装 Amlogic 官方刷机工具 目前只支持 windows 
           地址：https://doc.rokid-inc.com/kass/basic/main/kwin.jsp -->9.软件文档/Glass系统软件/Glass软件设计文档（团队内部）/二代眼镜/ Amlogic USB Burning Tool_v2.1.6.8.exe
        2. 选择刷机镜像。
            打开Amlogic 刷机工具，选择菜单中的”文件”->”导入烧录包”，在弹出的文件对话框中选择 “aml_upgrade_package.img” 文件。
        3. 进入刷机模式
            首先：开发板核心板 TypeC 接口与 PC 已连接。
                方法一：接上开发板配套的 Debug 板，按住 Debug 板上的boot 键然后上电，刷机工具界面出现 连接成功，即表示开发板已经进入刷机模式。
                方法二：通过 Debug 板上的 MicroUSB 连接串口线，进入u-boot，输入update，您将看到刷机工具界面出现连接成功，表示开发板已经进入刷机模式。
                方法三：如果您的开发板系统是 Android 系统，adb shell reboot update，您将看到刷机界面出现连接成功，表示开发板已经进入刷机模式。
        4. 开始刷机
            点击刷机工具中的”开始”按钮，开始刷机，并等待刷机完成，请按照刷机工具中的提示操作。

```



## *如何将CRWON上编译至W400公板使用

CROWN与W400公板所使用的DDR有所不同，因此Prebuild的Uboot需要更改。为了方便调试，我们增加了将CROWN编译至W400的功能，请通过如下方式进行编译：

```sh
export CROWN_W400_SUPPORT=true
source build/envsetup.sh
lunch crown-userdebug
make -j40
```

**注：CROWN_W400_SUPPORT生效时，内核所使用的DTS为g12b_a311d_w400，CONFIG为meson64_a32_defconfig/meson64_defconfig。**

