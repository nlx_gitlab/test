---
id: Guide To Code Modification Of Zhongyou Head Ring
title: 代码修改指南
--- 

# 系统代码修改集成说明

## device/vendor目录

* device目录新建头环产品目录“device/amlogic/crown”，复制自w400，有Rokid相关配置修改，请修改crown目录；
* 在crown下引用了vendor下的pluto.mk，新加的编译配置和模块配置，应加在vendor/rokid/pluto/pluto.mk。
* device下的common目录，是amlogic所有device共享的目录，如果要修改这个下面的编译配置，添加``ifeq ($(TARGET_ROKID_XR), true)``用以区分我们的修改。
* 总之一个大的原则就是，**Rokid新加的东西，加在vendor目录，修改原有配置的，或者只能改device目录的，修改“crown”目录，需保证不影响原公版的编译配置**。

## framework代码修改

* 修改原生framework代码的，必须添加Rokid字样的注释，示例如下：

```
                // Rokid Add :修改说明注释
                xxxx;
                xxx;
                // Rokid End
```
* 修改framework代码暂不考虑对w400公版的影响，通过代码分支控制；

## Kernel代码修改

* 内核代码修改需使用CONFIG进行限制，保证W400不受影响。
* Crown DeviceTree文件为：arch/arm64/boot/dts/amlogic/rokid-crown-evt.dts （ARM 64bit）和  arch/arm/boot/dts/amlogic/rokid-crown-evt.dts (ARM 32bit)
* Crown CONFIG文件为：arch/arm64/configs/crown64_defconfig 和 arch/arm/configs/crown32_defconfig



**需要注意的是：当前尚未确认最后使用的内核版本为ARM 还是ARM64，请确保在内核中的修改能够覆盖ARM与ARM64.**

## HAL 代码修改

@金龙补充

- 涉及自定义修改需添加Rokid相关注释，可参考framework代码注释
- fix bug的修改需要记录对应的bug和日期，平台版本更新引入的修改类似。



## Uboot代码修改

Uboot源码所在目录：bootloader/uboot-repo

引用外部工具链：export PATH=$PATH:/opt/gcc-linaro-aarch64-none-elf-4.8-2013.11_linux/bin/:/opt/CodeSourcery/Sourcery_G++_Lite/bin/

编译命令：./mk g12b_w400_v1 --systemroot

编译后将生成的bootloader.img  u-boot.bin.sd.bin  u-boot.bin.usb.bl2  u-boot.bin.usb.tpl 文件拷贝至device/amlogic/crown/upgrade/rokid目录（Amlogic的Uboot采用Prebuild的方式进行集成）。

