---
id: Glass Camera Firmware Upgrade Process
title: Glass固件升级流程
--- 
 
1. 升级任务添加流程图

   ![固件更新添加任务流程](./固件更新添加任务流程.jpg)

2. 升级任务管理流程图

   ![固件升级任务管理](./固件升级任务管理.jpg)

3. MCU、Camera任务执行流程

   ![固件升级MCU、Camera任务流程](./固件升级MCU、Camera任务流程.jpg)