---
id: Gerrit Sync Repo
title: Gerrit同步外部repo
--- 

### 一. repo仓库代码同步至gerrit

*   #### 1.build-8上repo sync一份与gerrit服务器格式相同的代码，即repo sync加-–mirror参数

    ```
    mkdir mirror
    cd mirror
    repo init --mirror -u https://android.googlesource.com/platform/manifest -b android-10.0.0_r2
    ```




*   #### 2.将代码拷贝到gerrit服务器(需要管理员权限）

    ```
    ssh root@ws11 
    rsync -avrp xhl@build-8:/home/xhl/mirror root@ws11:/root/glass3_code_tmp/
    ```

 

*   #### 3.在gerrit服务器上改变目录属主，拷贝到gerrit对应目录下,(目录结构要什么样就怎么放，目录名可以改变)

    ```
    ssh root@ws11
    chown -R openai-corp:openai-corp /home/root/glass3_code_tmp/mirror 
    cd /home/root/glass3_code_tmp/
    mv mirror /home/openai-corp/review_site/git/hi3796cv300-q
    ```

*   #### 4.用命令刷一下gerrit，刷完后在gerrit上即可看到代码，得有administrator权限才能刷

    [gerrit使用说明](https://osm.etsi.org/gerrit/Documentation/cmd-flush-caches.html)
    ```
    ssh -p 29418 hailaing.xu@openai-corp.rokid.com gerrit flush-caches --cache --list
    ssh -p 29418 hailaing.xu@openai-corp.rokid.com gerrit flush-caches --cache projects
    ssh -p 29418 hailiang.xu@openai-corp.rokid.com gerrit flush-caches --cache project_list
    ```



### 二.   gerrit project批量设置权限

*   #### 1.创建权限project，如rokid_glass2/rokid_glass2

    ```
    #!/bin/bash

    gerrit_server="openai-corp.rokid.com"
    basepath="rokid_glass3"
    ssh -p 29418 hailiang.xu@${gerrit_server} gerrit create-project ${basepath}/${basepath}.git --permissions-only
    if [ $? -ne 0 ];then
      echo "make group $basepath success"
    else
      echo "make group $basepath failed"
    fi
    ```
 

*   #### 2.给rokid_glass2/rokid_glass2配置上具体权限

    权限说明：
    [gerrit 权限说明](https://blog.51cto.com/zengestudy/1774176)

*   #### 3.给hi3796cv300-q开头所有的project加上rokid_glass2（沿用之前的权限）
    ```
    #!/bin/bash

    gerrit_server="openai-corp.rokid.com"
    basepath="rokid_glass2"
    projects=`ssh -p 29418 hailiang.xu@${gerrit_server} gerrit ls-projects | grep "^hi3796cv300"`
    for proj in ${projects}
    do
        ssh -p 29418 hailiang.xu@${gerrit_server} gerrit set-project-parent --parent ${basepath}/${basepath} ${proj}
        if [ $? -eq 0 ];then
            echo "$proj set parent  success"
        else
            echo "$proj set parent failed"
        fi

    done
    ```
    
    
### 三. 批量创建分支

```
    #!/bin/bash

    gerrit_server="openai-corp.rokid.com"
    newbranch="rokid_glass3"
    revision="refs/tags/android-10.0.0_r2"
    projects=`ssh -p 29418 hailiang.xu@${gerrit_server} gerrit ls-projects | grep "^hi3796cv300"`
    for proj in ${projects}
    do
        ssh -p 29418 hailiang.xu@${gerrit_server} gerrit create-branch  ${proj} ${newbranch} ${revision};
        if [ $? -eq 0 ];then
            echo "$proj create $newbranch success"
        else
            echo "$proj create branch failed"
        fi

    done
```


### 四.修改repo platform/manifest/ branch rokid_glass3 default.xml



```
diff --git a/default.xml b/default.xml
index 8a3edda..30139be 100644
--- a/default.xml
+++ b/default.xml
@@ -1,11 +1,11 @@
 <?xml version="1.0" encoding="UTF-8"?>
 <manifest>

-  <remote  name="aosp"
-           fetch=".."
-           review="https://android-review.googlesource.com/" />
-  <default revision="refs/tags/android-10.0.0_r2"
-           remote="aosp"
+  <remote  name="openai-corp"
+           fetch="../"
+           review="https://openai-corp.rokid.com" />
+  <default revision="rokid_glass3"
+           remote="openai-corp"
            sync-j="4" />

   <project path="build/make" name="platform/build" groups="pdk" >
```


### 五.下载代码
```
repo init -u ssh://username@openai-corp.rokid.com:29418/hi3796cv300-q/platform/manifest  -b rokid_glass3 --repo-url=ssh://username@openai-corp.rokid.com:29418/tools/repo --no-repo-verify
```
