---
id: Rokid Gradle Plugin
title: Rokid Gradle Plugin
--- 
 
主要用于 SDK 发布，该工具可以上传自定义 Maven 仓库和 JCenter
## 集成
### 根目录配置
在工程根目录的`build.gradle`中增加声明
``` groovy
buildscript {
    repositories {
        .......
        maven { url 'http://appmaven.rokid-inc.com/repository/maven-public/' }
    }
    dependencies {
        ......
        
        classpath 'com.rokid.build:gradle-plugin:1.3.5'
    }
}
```

### module配置 
在自己的 library 工程目录下的`build.gradle`增加以下配置
``` groovy
apply plugin: 'rokid-maven'
rokid {
    maven {
        groupId = "com.rokid.glass"
        artifactId = "ui" // module
        version = "1.0.0"

        // Release
        repository {
            url = "http://mvnrepo.rokid-inc.com/nexus/content/repositories/releases/" 
            sourcesJar = false
            javadocJar = false
        }

      	// Snapshots，用于开发阶段，依赖者 不用 每次修改版本号
        snapshotRepository {
            url = 'http://mvnrepo.rokid-inc.com/nexus/content/repositories/snapshots/'
            sourcesJar = true
            javadocJar = false
        }
      	
      	// bintray
        bintray {
            repo = "maven"
            userOrg = "rokidglass" // 组织名
            name = "XXXX"
            desc = 'XXXX'
            websiteUrl = "https://www.rokid.com"
            vcsUrl = "https://www.rokid.com"

            sourcesJar = false // 源码上传标识，fase 为 不上传
            javadocJar = false // 源码上传标识，fase 为 不上传
        }
    }
}

```
### Task
![](../media/plugin/rokid_plugin.png)
* mavenPublish：上传到自定义Maven的 Release 库
* mavenSnapshotPublish：上传到自定义Maven的 Snapshot 库
* bintrayPublish：上传到 bintray 库

### 账号配置
``` bash
# Maven
rokid.maven.userName=XXX
rokid.maven.password=XXX
# bintray
rokid.maven.bintray.user=XXX
rokid.maven.bintray.apiKey=XXX
```

### 引用上传的 Library
1. 工程根目录下 的 build.gradle 中添加以下配置

``` bash
allprojects {
    // Snapshot 库时使用，强制取消缓存，并更新依赖库
    configurations.all {
        resolutionStrategy.cacheChangingModulesFor 1, 'seconds'
    }

    repositories {
       ......
        maven { url 'http://mvnrepo.rokid-inc.com/nexus/content/groups/public/' }
    }
}
```
2. 使用Library库的声明，在 build.gradle增加配置   

使用Release库
``` bash
implementation 'com.rokid.glass:ui:1.0.0'
```

使用Snapshot库
``` bash
implementation ('com.rokid.glass:ui:1.0.0-SNAPSHOT') { changing = true }
```
