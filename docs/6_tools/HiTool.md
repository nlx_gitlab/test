---
id: HiTool
title: HiTool 下载工具使用
--- 

# 海思 HiTool 下载工具使用
介绍通过 HiTool 下载 3代Glass DOCK 固件方法
## HiTool 获取&安装
#### 获取
在 [Rokid 文档管理系统](https://doc.rokid-inc.com/kass/basic/main/kwin.jsp)获取最新的 `HiTool` 工具,路径：`/9.软件文档/Glass系统软件/Glass软件设计文档（团队内部）/三代眼镜/`
#### 安装
1   将获取的 HiTool 解压出来，到解压后的目录下直接点击 `HiTool.exe` 即可打开该下载工具  
2   打开工具后，首先弹出芯片选择界面，按如下截图所示，选择：`Hi3751V900`  
![](../media/HiTool/ChoiseIc.png)  
3   选择完芯片后，会进入欢迎界面，如下图所示，点击 `HiBurn`,进入下载页面  
![](../media/HiTool/welcome.png)
## 下载
下载所需硬件工具：  
> 1 USB转串口设备   
> 2 typec USB线  
 
驱动安装：
> 1 USB转串口驱动  
> 2 海思USB 驱动，在[Rokid 文档管理系统](https://doc.rokid-inc.com/kass/basic/main/kwin.jsp)获取驱动`adb_interface_usb_driver.zip`,路径：`/9.软件文档/Glass系统软件/Glass软件设计文档（团队内部）/三代眼镜/`  
> 注: 该驱动在win10平台可能存在问题，可以访问 https://www.filehorse.com/download-adb-drivers/ 更新adb驱动。

下载界面如下图所示(`7月31日有更新Hitool工具（5.4.0），支持仅通过 USB 下载`)：  
按如下步骤进行固件下载（对应`5.3.27 Hitool工具`）:
> <br/>1) 点击截图 1号红框 `刷新`，正常情况下能识别到插入的 usb 串口设备（若不能刷新出串口，则需检查串口是否有被正常插上？是否有装串口驱动？）
> <br/>2) 选择截图 2号红框 `串口+USB`传输方式
<br/>3) 点击截图 3号红框 `浏览`，找到所需下载固件的 xml 分区文件并打开
<br/>4) 截图 4号红框作用是`全部取消/选择`，正常下载完整固件时，直接进行第5步
<br/>5) 点击截图 5号红框 `烧写`，后 6号红框会显示*串口已经连接，请给单板上电，若已经上电，请断电后重新上电*
<br/>6）长按power键，等 截图 6号红框有 `COM`显示时，松开power键，即开始下载了 
 
![](../media/HiTool/Download.png)  
  
5.4.0版本Hitool工具使用：
> <br/>1) 通过 USB+串口下载，使用方法同`5.3.27 Hitool 工具`
> <br/>2) 7月31号后的固件版本支持脱离串口进行usb烧写。烧写方法：  
一、开机后执行 adb reboot bootloader   
二、如下图所示，选择`USB口`，然后点`烧写`即可
  
![](../media/HiTool/WX20200731-172626.png)  


