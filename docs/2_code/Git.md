---
id: Git Submission Specification
title: Git提交规范
--- 
  
---------------
## 基本规范

* 应用

```
[type][bugid]: subject      //注意":"后面有空格

body

footer
```

* 系统 & BSP

```
[type][scope]: subject      //注意":"后面有空格

body

footer
```

---------------

*   #### type(必填项）
    **type 用于说明 commit 的类别，只允许使用下面11个标识：**
    *   feat : 增加新功能（feature）
    *   fix : 修改bug，需要增加禅道ID，如[FIX][ID]: xxxx
    *   update : 模块更新，或者版本升级等其他更新
    *   perf : 性能优化
    *   revert : 撤销上一次的 commit
    *   build : 修改编译环境/工具等 
    *   docs : 修改文档
    *   style : 修改代码格式（不影响代码运行的变动）
    *   refactor : 重构（既不是新增功能，也不是修改bug的代码变动）
    *   test : 增加测试
    *   chore - 构建过程或辅助工具的变动
  
*   #### scope(可选项)
    **用于说明 commit 影响的范围，比如数据层、控制层、视图层等等，视项目不同而不同。**
    
*   #### subject(必填项）
    **commit 的简短描述。**
    **（不超过50个字符，建议使用英文，可以使用中文，且结尾不加句号）**

*   #### body（可选项）
    **commit 的具体描述。**
    
*   #### footer(可选项）
    **一些备注或者BUG的链接。**
    
## 示例

*   feat(WFD): add wifi display module
*   fix(WFD): fix video bitrate too low


