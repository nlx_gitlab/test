- 编码规范

    - [系统编码规范](2_code/System.md)
    - [Java编码规范](2_code/Java.md)
    - [Android编码规范](2_code/Android.md)
    - [Git提交规范](2_code/Git.md)